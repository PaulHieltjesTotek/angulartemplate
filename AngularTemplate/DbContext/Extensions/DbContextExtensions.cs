﻿using System;
using Core.Helpers;
using DbContext.ModelConfigurations;
using Microsoft.EntityFrameworkCore;

namespace DbContext.Extensions
{
    public static class DbContextExtensions
    {
        public static void ConfigurateDbContextEntities(this Microsoft.EntityFrameworkCore.DbContext dbContextTarget, ModelBuilder modelBuilder, string assembly = "Skeyes.DbContext")
        {
            ReflectionHelper reflectionHelper = new ReflectionHelper();
            foreach (var conf in reflectionHelper.GetAllClassesThatInheritFromInterface(typeof(IModelConfigurator), assembly))
            {
                var c = (IModelConfigurator) Activator.CreateInstance(conf);
                if(c.GetDbContextTarget() == dbContextTarget.GetType())
                c.Configurate(modelBuilder);
            }
        }
    }
}

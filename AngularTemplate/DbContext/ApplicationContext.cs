using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DbContext.Base;
using DbContext.Extensions;
using Domain.Base;
using Domain.Models.Application;
using Domain.Models.Auditing;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Shared.Models.Authentication;

namespace DbContext
{
    public class ApplicationContext : BaseAuditableContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options, IHttpContextAccessor accessor, UserManager<AppUser> userManager)
            : base(options, accessor, userManager)
        {
        }

        public DbSet<GridLayout> GridLayouts { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<PickList> PickLists { get; set; }
        protected override string Prefix => "Application";
    }
}
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Shared.Models.Authentication;

namespace DbContext
{
    public class IdentityContext : IdentityDbContext
    {

        public IdentityContext(DbContextOptions<IdentityContext> options)
            : base(options)
        {

        }

        public DbSet<AppUser> AppUsers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Property Configurations
            modelBuilder.UsePropertyAccessMode(PropertyAccessMode.Property);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DbContext.Base;
using DbContext.Extensions;
using Domain.Base;
using Domain.Models;
using Domain.Models.Auditing;
using Domain.Workflows;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Shared.Models.Authentication;

namespace DbContext
{
    public class DatabaseContext : BaseAuditableContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options, IHttpContextAccessor accessor, UserManager<AppUser> userManager)
            : base(options, accessor, userManager)
        {
        }
        public DbSet<DemoWorkflowModel> DemoWorkflows { get; set; }

        protected override string Prefix => "Main";
    }

}

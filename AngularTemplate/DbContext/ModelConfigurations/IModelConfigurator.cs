﻿using System;
using Microsoft.EntityFrameworkCore;

namespace DbContext.ModelConfigurations
{
    public interface IModelConfigurator
    {
        void Configurate(ModelBuilder builder);
        Type GetDbContextTarget();
    }
}

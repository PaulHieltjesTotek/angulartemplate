using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Shared.Models.Authentication;

namespace tomware.Microwf.AspNetCoreEngine.Services
{
  public interface IUserContextService
  {
    /// <summary>
    /// Returns the name of the current user.
    /// </summary>
    string UserName { get; }
  }

  public class UserContextService : IUserContextService
  {
    public static readonly string SYSTEM_USER = "SYSTEM";
    private readonly IHttpContextAccessor _context;
    private readonly UserManager<AppUser> _userManager;
    
    public string UserName
    {
      get
      {
        //var user = _userManager.Users.FirstOrDefault(u => u.UserName == _userManager.GetUserId(_context.HttpContext.User));
        var userName = _context.HttpContext != null 
          ? _userManager.GetUserId(_context.HttpContext.User)
          : SYSTEM_USER;
        if (string.IsNullOrWhiteSpace(userName))
          throw new InvalidOperationException("UserName is null!");

        return userName;
      }
    }

    public UserContextService(IHttpContextAccessor context, UserManager<AppUser> userManager)
    {
      _context = context;
      _userManager = userManager;
    }
  }
}
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using tomware.Microwf.AspNetCoreEngine.Common;
using tomware.Microwf.Core.Execution;

[assembly: InternalsVisibleTo("tomware.Microwf.Tests")]

namespace tomware.Microwf.AspNetCoreEngine.Domain
{
  [Table("Workflow")]
  public partial class Workflow
  {
    [Key]
    public Guid Id { get; set; }

    [Required]
    public string Type { get; set; }

    [Required]
    public string State { get; set; }

    [Required]
    public Guid CorrelationId { get; set; }

    public string Assignee { get; set; }

    public DateTime Started { get; set; }

    public DateTime? Completed { get; set; }

    public List<WorkflowVariable> WorkflowVariables { get; set; } = new List<WorkflowVariable>();

    public List<WorkflowHistory> WorkflowHistories { get; set; } = new List<WorkflowHistory>();
    
    public  List<WorkflowMessage> Messages { get; set; } = new List<WorkflowMessage>();

    internal static Workflow Create(
      Guid correlationId,
      string type,
      string state,
      string assignee,
      DateTime? dueDate = null
    )
    {
      return new Workflow
      {
        Started = SystemTime.Now(),
        CorrelationId = correlationId,
        Type = type,
        State = state,
        Assignee = assignee
      };
    }

    internal void AddVariable(WorkflowVariableBase variable)
    {
      var type = KeyBuilder.ToKey(variable.GetType());
      var existing = this.WorkflowVariables.FirstOrDefault(v => v.Type == type);
      if (existing != null)
      {
        existing.UpdateContent(variable);

        return;
      }

      this.WorkflowVariables.Add(WorkflowVariable.Create(this, variable));
    }

    internal void AddHistoryItem(string fromState, string toState, string userName, string message)
    {
      this.WorkflowHistories.Add(new WorkflowHistory
      {
        Created = SystemTime.Now(),
        FromState = fromState,
        ToState = toState,
        UserName = userName,
        WorkflowId = this.Id,
        Message = message,
        Workflow = this
      });
    }
    
    internal void AddMessageItem(string message, string userName)
    {
      if (string.IsNullOrWhiteSpace(message)) return;
      
      Messages.Add(new WorkflowMessage
      {
        AddedAt = SystemTime.Now(),
        WorkflowId = Id,
        Message = message,
        Username = userName
      });
    }
  }
}

using Microsoft.EntityFrameworkCore;

namespace tomware.Microwf.AspNetCoreEngine.Domain
{
  public class EngineDbContext : DbContext
  {
    public EngineDbContext(DbContextOptions options) : base(options)
    { }

    public DbSet<Workflow> Workflows { get; set; }
    public DbSet<WorkItem> WorkItems { get; set; }
    
    public DbSet<WorkflowHistory> WorkflowHistories { get; set; }
    public DbSet<WorkflowMessage> WorkflowMessages { get; set; }
  }
}

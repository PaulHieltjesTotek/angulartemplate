using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace tomware.Microwf.AspNetCoreEngine.Domain
{
  [Table("WorkflowHistory")]
  public partial class WorkflowHistory
  {
    [Key]
    public Guid Id { get; set; }

    [Required]
    public DateTime Created { get; set; }

    [Required]
    public string FromState { get; set; }

    [Required]
    public string ToState { get; set; }

    public string UserName { get; set; }
    
    public string Message { get; set; }

    public Guid WorkflowId { get; set; }

    [JsonIgnore]
    public Workflow Workflow { get; set; }
  }
}

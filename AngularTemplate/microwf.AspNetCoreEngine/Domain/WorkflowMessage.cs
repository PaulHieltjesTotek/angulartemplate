using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tomware.Microwf.AspNetCoreEngine.Domain
{
    [Table("WorkflowMessage")]
    public class WorkflowMessage
    {
        [Key]
        public Guid Id { get; set; }
        
        public Guid WorkflowId { get; set; }
        
        public DateTime AddedAt { get; set; }
        
        public string Username { get; set; }
        
        public string Message { get; set; }
    }
}
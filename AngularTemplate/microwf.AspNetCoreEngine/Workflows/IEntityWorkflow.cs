using System;
using tomware.Microwf.Core.Definition;

namespace tomware.Microwf.AspNetCoreEngine.Workflows
{
  public interface IAssignableWorkflow : IWorkflow
  {
    /// <summary>
    /// The assigned subject for a workflow.
    /// </summary>
    string Assignee { get; set; }
    
    /// <summary>
    /// The message added while assigning
    /// </summary>
    string Message { get; set; }
  }

  public interface IEntityWorkflow : IAssignableWorkflow
  {
    /// <summary>
    /// Id of an entity.
    /// </summary>
    Guid Id { get; set; }
  }
}

using System;
using tomware.Microwf.Core.Definition;

namespace tomware.Microwf.AspNetCoreEngine.Workflows
{
  public abstract class EntityWorkflowDefinitionBase : WorkflowDefinitionBase
  {
    public abstract Type EntityType { get; }
  }
}
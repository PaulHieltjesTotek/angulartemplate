using System;
using System.ComponentModel.DataAnnotations;

namespace tomware.Microwf.AspNetCoreEngine.Models
{
  public class WorkflowTriggerViewModel
  {
    [Required]
    public Guid Id { get; set; }

    [Required]
    public string Trigger { get; set; }
  }
}
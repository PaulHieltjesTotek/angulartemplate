using System;

namespace tomware.Microwf.AspNetCoreEngine.Models
{
  public class WorkflowViewModel
  {
    public Guid Id { get; set; }
    public Guid CorrelationId { get; set; }
    public string Type { get; set; }
    public string State { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Assignee { get; set; }
    public string Route { get; set; }
    public DateTime Started { get; set; }
    public DateTime? Completed { get; set; }
  }
}
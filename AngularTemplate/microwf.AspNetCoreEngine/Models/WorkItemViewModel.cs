using System;
using System.ComponentModel.DataAnnotations;

namespace tomware.Microwf.AspNetCoreEngine.Models
{
  public class WorkItemViewModel
  {
    [Required]
    public Guid Id { get; set; }

    public DateTime? DueDate { get; set; }
  }
}
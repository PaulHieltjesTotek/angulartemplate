using System;

namespace tomware.Microwf.AspNetCoreEngine.Messaging
{
  public class WorkItemMessage
  {
    public string TriggerName { get; set; }

    public Guid EntityId { get; set; }

    public string WorkflowType { get; set; }

    public static WorkItemMessage Create(string triggerName, Guid entityId, string workflowType)
    {
      return new WorkItemMessage
      {
        TriggerName = triggerName,
        EntityId = entityId,
        WorkflowType = workflowType
      };
    }
  }
}
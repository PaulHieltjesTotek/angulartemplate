namespace tomware.Microwf.AspNetCoreEngine.Common
{
  public class ProcessorConfiguration
  {
    public bool Enabled { get; set; }
    public int Interval { get; set; }
  }
}
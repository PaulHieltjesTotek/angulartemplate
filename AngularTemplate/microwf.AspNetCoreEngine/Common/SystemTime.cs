using System;

namespace tomware.Microwf.AspNetCoreEngine.Common
{
  public static class SystemTime
  {
    public static Func<DateTime> Now = () => DateTimeOffset.Now.DateTime;
  }
}
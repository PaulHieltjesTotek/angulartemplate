namespace tomware.Microwf.AspNetCoreEngine.Common
{
  public class Constants
  {
    public const string MANAGE_WORKFLOWS_POLICY = "manage_workflows_policy";
    public const string WORKFLOW_ADMIN_ROLE = "workflow_admin";

    public static int WORKITEM_RETRIES = 3;
  }
}
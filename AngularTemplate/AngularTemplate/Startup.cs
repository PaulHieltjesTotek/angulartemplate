using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using AutoMapper;
using Core;
using Core.Attributes;
using Core.Extensions;
using Core.Helpers;
using DbContext;
using Domain;
using Infrastructure.Services;
using Infrastructure.Services.Interfaces;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;
using NLog;
using NLog.Config;
using Persistence.DataSeeding;
using Persistence.OData;
using Persistence.Repositories;
using Persistence.Repositories.Interfaces;
using Shared.Models.Authentication;
using tomware.Microwf.AspNetCoreEngine;
using tomware.Microwf.AspNetCoreEngine.Common;

namespace AngularTemplate
{
    public class Startup
    {
        //change this when using this template: https://passwordsgenerator.net
        private const string SecretKey = "Nhyh8uU4Gj3aYQedR4UQmvzpHvQ8mAeR";
        private readonly SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
        
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
       public void ConfigureServices(IServiceCollection services)
        {
            var databaseProvider = (DatabaseProvider)Enum.Parse(typeof(DatabaseProvider),
                Configuration.GetSection("AppSettings").GetSection("DatabaseProvider").Value);

            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            #region authorization
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));

            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = false,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = true,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };


            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(j =>
            {
                j.TokenValidationParameters = tokenValidationParameters;
                j.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                j.ClaimsIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                
            });
            
            services.AddIdentity<AppUser, IdentityRole>
                (o =>
                {
                    // configure identity options
                    o.Password.RequireDigit = false;
                    o.Password.RequireLowercase = false;
                    o.Password.RequireUppercase = false;
                    o.Password.RequireNonAlphanumeric = false;
                    o.Password.RequiredLength = 6;
                })
                .AddEntityFrameworkStores<IdentityContext>()
                .AddDefaultTokenProviders();

            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser().Build());

                foreach (var role in typeof(UserRoles).GetAllPublicConstantValues<string>())
                {
                    auth.AddPolicy(
                        role,
                        policy => policy.RequireRole(role)
                    );
                }
            });
            
            #endregion
            
            services
                .AddMvc(o =>
                {
                    var policy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
                        .RequireAuthenticatedUser()
                        .Build();
                    o.Filters.Add(new AuthorizeFilter(policy));
                    o.EnableEndpointRouting = false;
                })
                .AddJsonOptions(
                    options =>
                    {
                        options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                        options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                    }
                )
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            
//            services.AddControllers().AddNewtonsoftJson(options =>
//            {
//                // Use the default property (Pascal) casing
//                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
//                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
//
//            });
            
            services.AddOData();
            
            services.AddAutoMapper();
            Mapper.Initialize(cfg => cfg.ValidateInlineMaps = false);
            
            
            //services.AddScoped<IGridLayoutRepository, GridLayoutRepository>();
            var types = System.Reflection.Assembly.Load("Infrastructure").GetTypes()
                .Where(t => !t.IsInterface).ToList();
            types.AddRange(System.Reflection.Assembly.Load("Persistence").GetTypes()
                .Where(t => !t.IsInterface));
            
            foreach (var mytype in types)
            {

                var autoDi = mytype.GetCustomAttributes(typeof(AutoDi), true).FirstOrDefault() as AutoDi;
                if(autoDi == null) continue;

                switch (autoDi.AddType)
                {
                    case AddType.Scoped:
                        if (autoDi.InterfaceType == null)
                        {
                            services.AddScoped(mytype);
                        }
                        else
                        {
                            services.AddScoped(autoDi.InterfaceType, mytype);
                        }
                        break;
                    case AddType.Transiant:
                        if (autoDi.InterfaceType == null)
                        {
                            services.AddTransient(mytype);
                        }
                        else
                        {
                            services.AddTransient(autoDi.InterfaceType, mytype);
                        }
                        break;
                    case AddType.Singleton:
                        if (autoDi.InterfaceType == null)
                        {
                            services.AddSingleton(mytype);
                        }
                        else
                        {
                            services.AddSingleton(autoDi.InterfaceType, mytype);
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

            }

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            
            services.AddDbContext<DatabaseContext>(options => options.UseSqlServer(
                Configuration.GetConnectionString("DefaultConnectionString"),
                b => b.MigrationsAssembly("Migrations.SQLServer")));
            services.AddDbContext<IdentityContext>(options => options.UseSqlServer(
                Configuration.GetConnectionString("DefaultConnectionString"),
                b => b.MigrationsAssembly("Migrations.SQLServer")));
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(
                Configuration.GetConnectionString("DefaultConnectionString"),
                b => b.MigrationsAssembly("Migrations.SQLServer")));
            services.AddDbContext<WorkFlowContext>(options => options.UseSqlServer(
                Configuration.GetConnectionString("DefaultConnectionString"),
                b => b.MigrationsAssembly("Migrations.SQLServer")));

            
            var workflowConf = CreateWorkflowConfiguration();
            IOptions<ProcessorConfiguration> processorConf = GetProcessorConfiguration(services);
            services
                .AddWorkflowEngineServices<WorkFlowContext>(workflowConf)
                .AddJobQueueServices<WorkFlowContext>(processorConf.Value);
        }
       
       private WorkflowConfiguration CreateWorkflowConfiguration()
       {
           //todo check with reflection which classes derive from EntityWorkflowDefinitionBase and auto create this list
           return new WorkflowConfiguration
           {
               Types = new List<WorkflowType> {
                  new WorkflowType {
                      Type = "DemoWorkflow",
                      Title = "Demo workflow",
                      Description = "Simple demo approval process.",
                      Route = "DemoWorkflow"
                  },
               }
           };
       }
       
       private IOptions<ProcessorConfiguration> GetProcessorConfiguration(IServiceCollection services)
       {
           var worker = Configuration.GetSection("Worker");
           services.Configure<ProcessorConfiguration>(worker);

           return services
               .BuildServiceProvider()
               .GetRequiredService<IOptions<ProcessorConfiguration>>();
       }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            
            app.UseAuthentication();
                
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseMiddleware<CultureHelper>();

            app.UseMvc(options =>
            {
                options.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
                options.Select().Expand().Filter().OrderBy().MaxTop(100).Count();
                options.MapODataServiceRoute("odata", "odata", EdmModelSetup.GetEdmModel());
            });

            app.UseSpa(spa =>
            {
                // To learn more about options for serving an Angular SPA from ASP.NET Core,
                // see https://go.microsoft.com/fwlink/?linkid=864501

                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }

            });
            
            AngularDatabaseSeeding.Initialize(app.ApplicationServices);
            LogManager.Configuration.Install(new InstallationContext());
           
            
            
        }
    }
}
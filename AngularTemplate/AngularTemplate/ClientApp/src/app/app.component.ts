import { Component } from '@angular/core';
import { locale, loadMessages, formatMessage } from 'devextreme/localization';
import 'devextreme-intl';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor() {
    locale('en-GB');
  }
}

export class StringHelper {
  public static padLeft(variable: string, size: number, char: string): string {
    while (variable.length < size) {
      variable = char + variable;
    }
    return variable;
  }

  public static padRight(variable: string, size: number, char: string): string {
    while (variable.length < size) {
      variable = variable + char;
    }
    return variable;
  }
}

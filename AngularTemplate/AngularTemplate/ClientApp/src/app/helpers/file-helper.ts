import { saveAs } from 'file-saver';

export class FileHelper {
  public static saveToFileSystem(data: any, filename: string) {
    saveAs(data, filename);
  }

  public static saveTextToFileSystem(data: string, filename: string) {
    const blob = new Blob([data], { type: 'text/plain' });
    saveAs(blob, filename);
  }
}

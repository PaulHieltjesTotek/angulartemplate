export class ArrayHelper {
  public static splitInChuncks(input: Array<any>, chunkSize: number): Array<Array<any>> {
    let output = [];
    let i,j;
    for (i = 0, j = input.length; i < j; i += chunkSize) {
      output.push(input.slice(i, i + chunkSize));
    }
    return output;
  }
}

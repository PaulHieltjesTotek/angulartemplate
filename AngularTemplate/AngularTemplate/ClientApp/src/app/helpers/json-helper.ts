export class JsonHelper
{
  public static deserialize(data: string): any
  {
    return JSON.parse(data, JsonHelper.reviveDateTime);
  }

  public static reviveDateTime(key: any, value: any): any
  {
    if (typeof value === 'string')
    {
      let a = /\/Date\((\d*)\)\//.exec(value);
      if (a)
      {
        return new Date(+a[1]);
      }
    }

    return value;
  }

  public static convertDatesOfObject(obj: any): any {
    Object.keys(obj).forEach(key => {
      obj[key] = JsonHelper.reviveDateTime(key, obj[key]);
    });
    return obj;
  }

}

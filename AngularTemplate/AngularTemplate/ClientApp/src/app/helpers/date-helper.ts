import { RangeSelection } from './models/range-selection';

export class DateHelper {
  public static addHours(date: Date, hours: number): Date {
    let result = new Date();
    result.setTime(date.getTime() + (hours*60*60*1000));
    return result;
  }

  public static addSeconds(date: Date, seconds: number): Date {
    let result = new Date();
    result.setTime(date.getTime() + (seconds*1000));
    return result;
  }

  public static addMiliSeconds(date: Date, miliSeconds: number): Date {
    let result = new Date();
    result.setTime(date.getTime() + (miliSeconds));
    return result;
  }

  public static setAsUtcDate(date: Date): Date {
    let result = new Date();
    result.setUTCFullYear(date.getFullYear(), date.getMonth(), date.getDate());
    result.setUTCHours(date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
    return result;
  }

  public static dateBetweenDates(check: Date, from: Date, to: Date): boolean {
    return (check.getTime() <= to.getTime() && check.getTime() >= from.getTime());
  }

  public static getRangeList(hourChunck: number, range: RangeSelection): RangeSelection[] {
    let result = [];

    let currentDate = new Date();
    currentDate.setUTCFullYear(range.startDate.getFullYear(), range.startDate.getMonth(),
      range.startDate.getDate());
    currentDate.setUTCHours(0, 0, 0, 0);

    let endDate = new Date();
    endDate.setUTCFullYear(range.endDate.getFullYear(), range.endDate.getMonth(),
      range.endDate.getDate());
    endDate.setUTCHours(0, 0, 0, 0);

    while (currentDate < endDate) {
      let newDate = DateHelper.addHours(currentDate, hourChunck);
      if (newDate > endDate) {
        newDate = endDate;
      }

      result.push(<RangeSelection> {
        startDate: currentDate,
        endDate: DateHelper.addMiliSeconds(newDate, -1)
      });

      currentDate = newDate;
    }

    return result;
  }
}

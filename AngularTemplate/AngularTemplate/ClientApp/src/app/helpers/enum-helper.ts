export class EnumHelper {

  public static convertEnumToKeyValue(enumarator: any): any {
    let map: { key: number; value: string }[] = [];

    for (let n in enumarator) {
      if (typeof enumarator[n] === 'number') {
        map.push({key: <any>enumarator[n], value: n.replace(/([A-Z])/g, ' $1').trim()});
      }
    }
    return map;
  }

}

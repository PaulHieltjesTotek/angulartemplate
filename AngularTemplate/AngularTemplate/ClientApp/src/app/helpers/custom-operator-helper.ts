export class CustomOperatorHelper {

  public static getCustomOperators(): Array<any> {
    return [{
      name: "yesterday",
      caption: "Yesterday",
      dataTypes: ["date"],
      icon: " fas fa-calendar-day",
      hasValue: false,
      calculateFilterExpression: function (a, b) {
        let today = new Date();
        today.setHours(0,0,0,0);
        let yesterday = new Date();
        yesterday.setDate(today.getDate() -1);
        yesterday.setHours(0,0,0,0);

        return [[b.dataField, ">", yesterday], "and", [b.dataField, "<", today]];
      }
    }];
  }

}

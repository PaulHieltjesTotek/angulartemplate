import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { DxFormComponent } from 'devextreme-angular';
import { confirm } from 'devextreme/ui/dialog';
import { AuthenticationService } from '../../services/authentication.service';
import { UserRolesConstants } from '../../user-roles';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['./user-modal.component.css']
})
export class UserModalComponent implements OnInit {

  user: User;
  visible: boolean;
  roleOptions: any;

  @Input()
  isFromUserManager: boolean = false;

  private resultObs$: Subscriber<boolean>;

  constructor(private authenticationService: AuthenticationService) {
    this.visible = false;
    this.close = this.close.bind(this);
    this.save = this.save.bind(this);
    this.user = new User();
  }

  ngOnInit() {

    this.roleOptions = {
      dataSource: UserRolesConstants.getAllRoles(),
      searchEnabled: true,
    };
  }

  show() : Observable<boolean> {
    this.visible = true;

    return new Observable<boolean>(
      (obs) => {
        this.resultObs$ = obs;
        this.user = new User();
      }
    );
  }

  save(form: DxFormComponent) {

    if(!form.instance.validate().isValid) {
      return;
    }
    let result = confirm(`Are you sure you wish to create this user?`, 'Confirm create');
    result.then((dialogResult) => {
      if (dialogResult) {

        if (this.isFromUserManager) {
          this.user.activate = true;
        } else {
          this.user.activate = false;
        }

        this.authenticationService.post<boolean>(`api/user/CreateUser`, this.user)
          .subscribe((result) => {
            if(result) {
              this.close(true);
            }
          });
      }
    });
  }

  close(success: boolean) {
    this.visible = false;
    this.resultObs$.next(success);
    this.resultObs$.complete();
    this.resultObs$.unsubscribe();
  }

}

export class User {
  public email: string;
  public userName: string;
  public initialRole: string;
  public activate: boolean;
}

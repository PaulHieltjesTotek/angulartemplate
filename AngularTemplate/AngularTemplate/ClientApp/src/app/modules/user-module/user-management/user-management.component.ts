import { Component, Inject, ViewChild } from '@angular/core';
import { CustomOperatorHelper } from '../../../helpers/custom-operator-helper';
import { AuthenticationService } from '../services/authentication.service';
import { DxDataGridComponent } from 'devextreme-angular';
import { confirm } from 'devextreme/ui/dialog';
import { UserModalComponent } from './user-modal/user-modal.component';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent {

  dataSource: any;
  popupPosition: any;
  customOperations: Array<any>;
  rolesDataSourceStorage: any;
  addVisible: boolean;
  roleSelectionUserId: any;
  addRolesDatasource: Array<any>;
  isLoading: boolean;

  @ViewChild(UserModalComponent)
  userModal: UserModalComponent;

  @ViewChild(DxDataGridComponent)
  dxDataGrid: DxDataGridComponent;

  constructor(private authenticationService: AuthenticationService) {
    this.init();
    this.isLoading = false;
    this.toggleActive = this.toggleActive.bind(this);
    this.delete = this.delete.bind(this);
    this.removeRoleFromUser = this.removeRoleFromUser.bind(this);
  }

  private init() {
    this.dataSource = this.authenticationService.getOdataStore('odata/ODataUser');
    this.popupPosition = { my: 'top', at: 'top', of: window };
    this.customOperations = CustomOperatorHelper.getCustomOperators();
    this.rolesDataSourceStorage = [];
    this.addVisible = false;
    this.addRolesDatasource = [];
  }

  getRoles(id: any) {
    let item = this.rolesDataSourceStorage.find((i) => i.key === id);
    if (!item) {

      item = {
        key: id,
        dataSourceInstance: []
      };

      this.rolesDataSourceStorage.push(item);

      this.authenticationService.get<any[]>('api/User/GetRolesForUser/' + id)
        .subscribe((r) => {
          if(r) {
            for(let i of r) {
              item.dataSourceInstance.push({userId: id, role: i});
            }
          }
        });
    }

    return item.dataSourceInstance;
  }

  toggleActive(e) {
    e.event.preventDefault();

    let result = confirm(`Are you sure you wish to change the activeness of this user?`, 'Confirm change active');
    result.then((dialogResult) => {
      if (dialogResult) {
        this.authenticationService.get<boolean>(`api/User/SetActivenessUser/${e.row.data.id}/${!e.row.data.active}`)
          .subscribe(
            (result) => {
              this.dxDataGrid.instance.refresh();
            }
          );
      }
    });


  }

  delete(e) {
    e.event.preventDefault();

    let result = confirm(`Are you sure you wish to delete this user?`, 'Confirm delete');
    result.then((dialogResult) => {
      if (dialogResult) {
        this.authenticationService.delete<boolean>(`api/User/DeleteUser/${e.row.data.id}`)
          .subscribe(
            (result) => {
              this.dxDataGrid.instance.refresh();
            }
          );
      }
    });


  }

  onToolbarPreparingUser (e) {
    let toolbarItems = e.toolbarOptions.items;

    // Adds a new item
    toolbarItems.push({
      widget: 'dxButton',
      options: { icon: 'add', onClick: (a) => {
        this.addUser(a, e);
        } },
      location: 'after'
    });
  }

  public addUser(e, b) {
    this.userModal.show()
      .subscribe((r) => {
        if(r) {
          this.dxDataGrid.instance.refresh();
        }
      });
  }

  onToolbarPreparing(e, userId: any) {
    let toolbarItems = e.toolbarOptions.items;

    // Adds a new item
    toolbarItems.push({
      widget: 'dxButton',
      options: { icon: 'add', onClick: () => {
          this.addRolesToUser(userId);
        }},
      location: 'after'
    });
  }

  public addRolesToUser(userId: any) {
    this.roleSelectionUserId = userId;
    this.addRolesDatasource.length = 0;
    this.authenticationService.get<any[]>('api/User/GetMissingRolesForUser/' + this.roleSelectionUserId)
      .subscribe((r) => {
        if(r) {
          for(let role of r) {
            this.addRolesDatasource.push({userId: userId, role: role});
          }
          this.addVisible = true;
        }
      });
  }

  public removeRoleFromUser(e) {
    e.event.preventDefault();
    let result = confirm(`Are you sure you wish to remove the ${e.row.data.role} role from this user?`, 'Confirm remove');
    result.then((dialogResult) => {
      if (dialogResult) {
        this.authenticationService.get<boolean>('api/User/RemoveRoleFromUser/' + e.row.data.userId + '/' + e.row.data.role)
          .subscribe(
            (r) => {
              let item = this.rolesDataSourceStorage.find((i) => i.key === e.row.data.userId);
              //remove element from details
              const index = item.dataSourceInstance.indexOf(e.row.data, 0);
              if (index > -1) {
                item.dataSourceInstance.splice(index, 1);
              }

            }
          );
      }
    });
  }

  public closePopup() {
    this.addVisible = false;
  }

  public async addSelectedRolesToUser(addRolesGrid: DxDataGridComponent) {
    this.isLoading = true;
    for (let role of addRolesGrid.selectedRowKeys) {
      this.authenticationService.get<boolean>('api/User/AddRoleToUser/' + this.roleSelectionUserId + '/' + role)
        .subscribe((r) => {
          if(r) {
            //add to detail grid
            let item = this.rolesDataSourceStorage.find((i) => i.key === this.roleSelectionUserId);
            item.dataSourceInstance.push({userId: this.roleSelectionUserId, role: role});


          }
        });
      await this.delay(500);
    }

    this.isLoading = false;
    this.addVisible = false;

    await this.delay(1000);
    this.dxDataGrid.instance.refresh();
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

}

export const UserRolesConstants = {
  adminRole: "admin",
  workflowAdmin: "workflow_admin",

  getAllRoles: () => {
    return [
      UserRolesConstants.adminRole,
      UserRolesConstants.workflowAdmin,
    ]
  },
};

import { Component, HostListener } from '@angular/core';
import { ICredentials } from '../models/icredentials';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  innerHeight: number = 500;
  credentials: ICredentials = { username: '', password: '' };
  loadIndicatorVisible: boolean = false;

  constructor(private authenticationService: AuthenticationService, private router: Router, private toastr: ToastrService) {
    this.innerHeight = window.innerHeight;

    if(this.authenticationService.isLoggedIn())
    {
      this.router.navigate(['/Protected/' + this.authenticationService.landingPage]);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = window.innerHeight;
  }

  forgotPassword() {
    this.router.navigate(['/ForgotPassword']);

  }

  login({ value, valid }: { value: ICredentials, valid: boolean }) {
    if (valid && !this.loadIndicatorVisible) {
      this.loadIndicatorVisible = true;
      this.authenticationService.login(value.username, value.password)
        .subscribe(
          result => {
            this.loadIndicatorVisible = false;
            if (result) {
              this.router.navigate(['/Protected/' + this.authenticationService.landingPage]);
            }
          });
    }
  }

}

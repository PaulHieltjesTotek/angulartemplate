import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private authenticationService: AuthenticationService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    let roles = route.data["roles"] as Array<any>;
    for (let r of roles) {
      //check if r is an array
      if (r instanceof Array) {
        for (let s of r) {
          if (this.authenticationService.userHasRole(s)) {
            return true;
          }
        }
      } else {
        if (this.authenticationService.userHasRole(r)) {
          return true;
        }
      }
    }

    return false;
  }
}

import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { ReturnModel } from '../models/return-model';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import "devextreme/data/odata/store";
import * as AspNetData from 'devextreme-aspnet-data-nojquery';
import CustomStore from 'devextreme/data/custom_store';
import ODataStore from "devextreme/data/odata/store";
import * as jwt_decode from 'jwt-decode';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class AuthenticationService {

  private localTokenName = 'auth_token';
  private localTokenExpiration = 'auth_expiration';
  private localTokenUsername = 'auth_username';
  private payloadRole = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role';
  private readonly basicHeaders: HttpHeaders;
  private readonly urlLanguage: string;

  public set language(locale: string) {

    if(locale == null) {
      return;
    }
    localStorage.setItem("locale", locale.toLowerCase());

    this.translate.use(locale);

    if(this.urlLanguage != locale) {
      parent.document.location.href = `/${locale}/`;
    }
  }

  public get language(): string {
    let l = localStorage.getItem("locale");
    return l == null ? 'en' : l;
  }

  private get authorizeHeaders(): HttpHeaders {
    return new HttpHeaders(this.authorizeHeadersObj);
  }

  private get authorizeHeadersObj(): any {
    return {
      'Content-Type': 'application/json',
      'Language': this.language,
      'Authorization': `Bearer ${localStorage.getItem(this.localTokenName)}`};
  }

  public set landingPage(page: string) {
    localStorage.setItem('landing_page', page);
  }

  public get landingPage(): string {
    let page = localStorage.getItem('landing_page');
    return page == null ? 'ToDoCardRequest' : page;
  }

  public set firstLogin(firstLogin: boolean) {
    localStorage.setItem('first_login', firstLogin ? 'true' : 'false');
  }

  public get firstLogin(): boolean {
    let firstLogin = localStorage.getItem('first_login');
    return firstLogin === 'true';
  }

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private router: Router,
              private toastr: ToastrService, private translate: TranslateService, private route: ActivatedRoute,
              private location: Location) {

      this.basicHeaders = new HttpHeaders({
        'Content-Type': 'application/json'
      });

  }

  public refreshCurrentPage() {
    window.location.href =`${this.baseUrl}${this.language}${this.location.path()}`;
  }

  public getBaseUrl(): string {
    return this.baseUrl;
  }

  public getAuthToken(): string {
    return `Bearer ${localStorage.getItem(this.localTokenName)}`;
  }

  public register(email: string, password: string, firstName: string, lastName: string,location: string, landingPage: string): Observable<any> {
    let body = JSON.stringify({ email, password, firstName, lastName, location, landingPage });
    let options = { headers: this.basicHeaders };

    return this.http.post<ReturnModel>(this.baseUrl + "api/User/Register", body, options);
  }

  public login(userName: string, password: string) {
    let options = { headers: this.basicHeaders };

    return new Observable<boolean>((observer) => {
      this.http
        .post<ReturnModel>(this.baseUrl + 'api/User/Login',
          JSON.stringify({ userName, password }),
          options
        ).subscribe(r => {

          if(r.isSuccessful) {
            const res = JSON.parse(r.result);
            localStorage.setItem(this.localTokenName, res.auth_token);
            localStorage.setItem(this.localTokenUsername, res.name);
            this.landingPage = res.landing_page;
            this.firstLogin = res.first_login;

            let exp = new Date();
            exp.setSeconds(exp.getSeconds() + (res.expires_in - 15));//set 15 seconds earlier so we can log out in time
            localStorage.setItem(this.localTokenExpiration, JSON.stringify(exp));
            observer.next(true);
          } else {
            for (let e of r.errors) {
              this.toastr.error(e);
            }
            observer.next(false);
          }

          observer.complete();
          observer.unsubscribe();
      }, (error) => {
        this.toastr.error(error.message)
      });
    });
  }

  public navigateToDefault(): void {
    this.router.navigate(['/Protected/' + this.landingPage]);
  }

  public userHasRole(role: string): boolean {
    const payload = this.getDecodedAccessToken();

    if (payload == null) return false;

    let roles = payload[this.payloadRole];

    if (roles instanceof Array) {
      return roles.filter(r => r === role).length > 0;
    } else {
      return roles === role;
    }

  }

  public hasOneOfRoles(roles: Array<any>): boolean {

    for (let r of roles) {
      if (r instanceof Array) {
        for (let s of r) {
          if (this.userHasRole(s)) {
            return true;
          }
        }
      } else {
        if (this.userHasRole(r)) {
          return true;
        }
      }
    }

    return false;
  }

  public logout(): void {
    localStorage.removeItem(this.localTokenName);
    this.router.navigate(['/Login']);

  }

  public getLoggedInUserName(): string {
    return localStorage.getItem(this.localTokenUsername);
  }

  public isLoggedIn(): boolean {
    const isTokenSet = !!localStorage.getItem(this.localTokenName);

    if(!isTokenSet) {
      return false;
    }

    //now also check expiration
    const exp = new Date(JSON.parse(localStorage.getItem(this.localTokenExpiration)));
    const isExpired = new Date() > exp;

    if(isExpired) {
      this.logout();
      return false;
    }

    return true;
  }

  private getDecodedAccessToken(): any {
    try{
      return jwt_decode(localStorage.getItem(this.localTokenName));
    }
    catch(Error){
      return null;
    }
  }

  public getOdataStore(url: string, keyType?: any): ODataStore {
    if (keyType == null) {
      keyType = {
        id: "Guid"
      };
    }

    let source =
      new ODataStore({
        key: 'id',
        keyType: keyType,
        url: this.baseUrl + url,
        version: 4,
        beforeSend: (e) => {
          // e.headers = {
          //   'Content-Type': 'application/json',
          //   'Authorization': `Bearer ${localStorage.getItem(this.localTokenName)}`
          // };
          e.headers = this.authorizeHeadersObj
        }
      });

    return source;
  }

  public getOdataStoreWithFilter(url: string, filter?: any, keyType?: any): any {
    if (keyType == null) {
      keyType = {
        id: "Guid"
      };
    }

    let source = {
      filter: filter,
      store: new ODataStore({
        key: 'id',
        keyType: keyType,
        url: this.baseUrl + url,
        version: 4,
        beforeSend: (e) => {
          e.headers = this.authorizeHeadersObj;
        }
      })};

    return source;
  }

  public getRestStore(url: string, key: string): any {

    let source = AspNetData.createStore({
      key: key,
      loadUrl: url,
      onBeforeSend: (method, ajaxOptions) => {
        ajaxOptions.headers = this.authorizeHeadersObj;
      }
    });

    return source;
  }

  public getCustomStore(request: Observable<any>): any {
    return new CustomStore({
      load: (loadOptions: any) => {
        return request
          .toPromise()
          .then((data: any) => {
            if(data) {
              return {
                data: data,
                totalCount: data.length
              }
            }
            return [];
          })
          .catch(error => { throw 'Data Loading Error' });
      }
    });
  }

  public getStream(url: string) {

    let options = {
      headers: this.authorizeHeaders,
      'responseType': 'blob' as 'json'
    };

    return this.http.get(this.baseUrl + url, options);
  }

  public get<T>(url: string): Observable<T> {
    let headers = this.authorizeHeaders;
    let options = { headers: headers };

    return new Observable<T>((observer) => {
      this.http.get<ReturnModel>(this.baseUrl + url,
          options
        ).subscribe(r => {

        if(r.isSuccessful) {
          //const res = JSON.parse(r.result);
          observer.next(r.result);//JsonHelper.convertDatesOfObject(r.result));
        } else {
          observer.next(null);
          this.toastr.error(r.errors.join('\n'));
        }

        observer.complete();
        observer.unsubscribe();
      }, (error) => {
        this.toastr.error(error);
        observer.next(null);
        observer.complete();
        observer.unsubscribe();
      });
    });

  }

  public postStream(url: string, obj: any) {

    let options = {
      headers: this.authorizeHeaders,
      'responseType': 'blob' as 'json'
    };

    return this.http.post(this.baseUrl + url, obj, options);
  }

  public post<T>(url: string, obj: any): Observable<T> {
    let headers = this.authorizeHeaders;
    let options = { headers: headers };

    return new Observable<T>((observer) => {
      this.http.post<ReturnModel>(this.baseUrl + url,
        JSON.stringify(obj),
        options
      ).subscribe(r => {

        if(r.isSuccessful) {
          //const res = JSON.parse(r.result);
          observer.next(r.result);//JsonHelper.convertDatesOfObject(r.result));
        } else {
          observer.next(null);
          for (let e of r.errors) {
            this.toastr.error(e);
          }
          //r.errors.join('\n'));
        }

        observer.complete();
        observer.unsubscribe();
      }, (error) => {
        this.toastr.error(error);
        observer.next(null);
        observer.complete();
        observer.unsubscribe();
      });
    });

  }

  public put<T>(url: string, obj: any): Observable<T> {
    let headers = this.authorizeHeaders;
    let options = { headers: headers };

    return new Observable<T>((observer) => {
      this.http.put<ReturnModel>(this.baseUrl + url,
        JSON.stringify(obj),
        options
      ).subscribe(r => {

        if(r.isSuccessful) {
          observer.next(r.result);
        } else {
          observer.next(null);
          for (let e of r.errors) {
            this.toastr.error(e);
          }
        }

        observer.complete();
        observer.unsubscribe();
      }, (error) => {
        this.toastr.error(error);
        observer.next(null);
        observer.complete();
        observer.unsubscribe();
      });
    });

  }

  public delete<T>(url: string): Observable<T> {
    let headers = this.authorizeHeaders;
    let options = { headers: headers };

    return new Observable<T>((observer) => {
      this.http.delete<ReturnModel>(this.baseUrl + url,
        options
      ).subscribe(r => {

        if(r.isSuccessful) {
          observer.next(r.result);
        } else {
          observer.next(null);
          for (let e of r.errors) {
            this.toastr.error(e);
          }
        }

        observer.complete();
        observer.unsubscribe();
      }, (error) => {
        this.toastr.error(error);
        observer.next(null);
        observer.complete();
        observer.unsubscribe();
      });
    });

  }

}

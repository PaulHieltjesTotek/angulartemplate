export class ReturnModel {

  public result: any;

  public isSuccessful: boolean;

  public errors: string[];

}

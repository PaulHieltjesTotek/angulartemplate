export class ChangePasswordViewModel {
  currentPassword?: string | undefined;
  newPassword?: string | undefined;
  confirmPassword?: string | undefined;
}

import { Component, HostListener, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { ChangePasswordViewModel } from '../models/models';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent {

  public changePasswordData: ChangePasswordViewModel;
  innerHeight: number = 500;
  loadIndicatorVisible: boolean = false;

  constructor(private authenticationService: AuthenticationService, private toastr: ToastrService,
              private translateService: TranslateService,  private router: Router) {
    this.changePasswordData = new ChangePasswordViewModel();
    this.innerHeight = window.innerHeight;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = window.innerHeight;
  }

  public changePassword({ value, valid }: { value: ChangePasswordViewModel, valid: boolean }) {
    if (valid && !this.loadIndicatorVisible) {
      this.loadIndicatorVisible = true;

      this.authenticationService.post<boolean>('api/User/ChangePassword', value)
        .subscribe((result) => {
          if (result) {
            this.toastr.success(this.translateService.instant('Password changed.'));
            this.authenticationService.login(this.authenticationService.getLoggedInUserName(), value.newPassword)
              .subscribe(
                result => {
                  if (result) {
                    this.router.navigate(['/Protected/' + this.authenticationService.landingPage]);
                  }
                });
          }
          this.loadIndicatorVisible = false;
        });
    }
  }

}

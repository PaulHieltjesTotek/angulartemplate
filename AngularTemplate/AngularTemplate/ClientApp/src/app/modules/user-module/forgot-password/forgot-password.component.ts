import { Component, HostListener, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {

  innerHeight: number = 500;
  loadIndicatorVisible: boolean = false;
  usernameVariable: string;

  constructor(private authenticationService: AuthenticationService, private toastr: ToastrService,
              private translateService: TranslateService,  private router: Router) {
    this.innerHeight = window.innerHeight;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = window.innerHeight;
  }

  public forgotPassword({ value, valid }: { value: any, valid: boolean }) {
    if (valid && !this.loadIndicatorVisible) {
      this.loadIndicatorVisible = true;

      this.authenticationService.get<boolean>('api/User/ResetPassword/' + value.username)
        .subscribe((result) => {
          if (result) {
            this.toastr.success(this.translateService.instant('Password reset.'));
            this.router.navigate(['/Login']);
          }
          this.loadIndicatorVisible = false;
        });
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './services/authentication.service';
import { AuthGuard } from './guard/auth-guard';
import { LoginGuard } from './guard/login-guard';
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import {
  DxButtonModule,
  DxDataGridModule,
  DxFormModule,
  DxLoadIndicatorModule, DxLoadPanelModule, DxPopupModule, DxScrollViewModule,
  DxTemplateModule, DxTextAreaModule,
  DxTextBoxModule
} from 'devextreme-angular';
import { RoleGuard } from './guard/role-guard';
import { UserManagementComponent } from './user-management/user-management.component';
import { ProtectedComponent } from '../template-module/protected/protected.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ChangePasswordGuard } from './guard/change-password-guard';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { UserModalComponent } from './user-management/user-modal/user-modal.component';
import { UserRolesConstants } from './user-roles';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DxTextBoxModule,
    DxTemplateModule,
    DxLoadIndicatorModule,
    DxDataGridModule,
    DxFormModule,
    DxPopupModule,
    DxTextAreaModule,
    DxLoadPanelModule,
    DxButtonModule,
    DxScrollViewModule,
    TranslateModule.forRoot(),
    RouterModule.forRoot([
      { path: '', redirectTo: 'Login', pathMatch: 'full' },
      { path: 'Login', component: LoginComponent },
      { path: 'ForgotPassword', component: ForgotPasswordComponent },
      { path: 'ChangePassword', component: ChangePasswordComponent, canActivate: [ChangePasswordGuard] },
      { path: 'Protected', component: ProtectedComponent, canActivate: [AuthGuard],
        children: [
          { path: 'UserManagement', component: UserManagementComponent, canActivate: [RoleGuard], data: { roles: [UserRolesConstants.adminRole]} }
        ]
      }
    ])
  ],
  declarations: [
    LoginComponent,
    UserManagementComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,
    UserModalComponent
  ], exports: [
    LoginComponent,
    UserModalComponent
  ],
  providers: [
    AuthenticationService,
    LoginGuard,
    AuthGuard,
    ChangePasswordGuard,
    RoleGuard
  ],
})
export class UserModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridLayoutComponent } from './grid-layout/grid-layout.component';
import { UserModule } from '../user-module/user.module';
import {
  DxButtonGroupModule, DxButtonModule,
  DxDataGridModule,
  DxDropDownBoxModule, DxFormModule, DxLoadIndicatorModule, DxLoadPanelModule,
  DxPopupModule, DxSelectBoxModule, DxTemplateModule,
  DxTextBoxModule
} from 'devextreme-angular';
import { FormsModule } from '@angular/forms';
import { BaseCrudGridComponent } from './base-crud-grid/base-crud-grid.component';

@NgModule({
  imports: [
    CommonModule,
    UserModule,
    DxDropDownBoxModule,
    DxTextBoxModule,
    DxPopupModule,
    DxButtonGroupModule,
    DxFormModule,
    DxButtonModule,
    DxSelectBoxModule,
    DxTemplateModule,
    DxDataGridModule,
    DxLoadIndicatorModule,
    FormsModule,
    DxLoadPanelModule
  ],
  declarations: [
    GridLayoutComponent,
    BaseCrudGridComponent
  ],
  exports: [
    GridLayoutComponent,
    BaseCrudGridComponent
  ]
})
export class CustomGridModule { }

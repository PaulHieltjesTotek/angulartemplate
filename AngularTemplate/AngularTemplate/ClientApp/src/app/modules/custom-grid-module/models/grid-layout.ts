
export class GridLayout {

  public id: string;
  public gridId: string;
  public layoutName: string;
  public layout: string;
  public userId: string;

}

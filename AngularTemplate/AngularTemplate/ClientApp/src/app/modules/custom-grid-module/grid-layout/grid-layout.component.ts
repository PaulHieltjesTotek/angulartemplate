import { Component, Input, OnInit } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { ToastrService } from 'ngx-toastr';
import { confirm } from 'devextreme/ui/dialog';
import { AuthenticationService } from '../../user-module/services/authentication.service';
import { GridLayout } from '../models/grid-layout';

@Component({
  selector: 'app-grid-layout',
  templateUrl: './grid-layout.component.html',
  styleUrls: ['./grid-layout.component.css']
})
export class GridLayoutComponent implements OnInit {

  private _dxDataGrid: DxDataGridComponent;

  @Input()
  public set dxDataGrid(grid: DxDataGridComponent) {
    this._dxDataGrid = grid;
    if (grid != null) {
      this.initGridStorting();
    }
  }

  @Input()
  gridId: string;

  layoutName: string;

  layouts: GridLayout[];

  currentLayout: GridLayout;

  layoutNameVisible: boolean;

  saveAsOptions: any;

  constructor(private authenticationService: AuthenticationService, private toastr: ToastrService) {
    this.layoutName = '';
    this.layoutNameVisible = false;
    this.saveAsOptions = {
      text: 'Save',
      onClick: this.saveCurrentLayout.bind(this)
    };
  }

  ngOnInit() {
    this.authenticationService.get<GridLayout[]>('api/GridLayout/' + this.gridId)
      .subscribe((r) => {
        if (r) {
          this.layouts = r;
        }
      });
  }

  public changeLayout(e): void {
    this.currentLayout = e.value;
    this.saveState(JSON.parse(this.currentLayout.layout));
    this._dxDataGrid.instance.state(this.loadState());
  }

  private initGridStorting(): void {
    this._dxDataGrid.stateStoring.enabled = true;
    this._dxDataGrid.stateStoring.type = 'custom';
    this._dxDataGrid.stateStoring.customLoad = this.loadState.bind(this);
    this._dxDataGrid.stateStoring.customSave = this.saveState.bind(this);
  }

  public loadState(): any {
    return JSON.parse(localStorage.getItem(this.gridId));
  }

  public saveState(state): void {
    localStorage.setItem(this.gridId, JSON.stringify(state));
  }

  public deleteLayout(layout: GridLayout): void {
    let result = confirm(`Are you sure you wish to delete the ${layout.layoutName} layout?`, 'Confirm delete');
    result.then((dialogResult) => {
      if(dialogResult) {
        this.authenticationService.delete('api/GridLayout/' + layout.id).subscribe(
          (r) => {
            if (r) {
              this.layouts.splice(this.layouts.indexOf(layout), 1);
              this.toastr.success('Layout deleted');
            }
          }
        );
      }
    });
  }

  public saveLayout(): void {
    let result = confirm(`Are you sure you wish to save over the ${this.currentLayout.layoutName} layout?`, 'Confirm save');
    result.then((dialogResult) => {
      if(dialogResult) {
        this.currentLayout.layout = localStorage.getItem(this.gridId);
        this.authenticationService.put('api/GridLayout', this.currentLayout).subscribe(
          (r) => {
            if (r) {
              this.toastr.success('Layout saved');
            }
          }
        );
      }
    });
  }
  public saveLayoutAs(): void {
    this.layoutName = '';
    this.layoutNameVisible = true;
  }

  public saveCurrentLayout(): void {
    //first check if the layoutname is valid
    if (this.layoutName == null || this.layoutName == '') {
      this.toastr.error('Layout name cannot be empty!');
      return;
    }

    if (this.layouts.filter(l => l.layoutName == this.layoutName).length > 0) {
      this.toastr.error('Layout name already in use!');
      return;
    }

    this.layoutNameVisible = false;
    let layout = new GridLayout();
    layout.layout = localStorage.getItem(this.gridId);
    layout.layoutName = this.layoutName;
    layout.gridId = this.gridId;
    this.authenticationService.post<GridLayout>('api/GridLayout', layout)
      .subscribe((r) => {
        if (r) {
          this.layouts.push(r);
          this.toastr.success('Layout saved');
        }
      });
  }

}

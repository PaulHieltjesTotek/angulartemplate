import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AuthenticationService } from '../../user-module/services/authentication.service';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
  selector: 'app-base-crud-grid',
  templateUrl: './base-crud-grid.component.html'
})
export class BaseCrudGridComponent {

  public dataSource: Array<any>;
  public settings: GridSettings;

  public isLoading: boolean;

  @Input()
  public set gridSettings(settings: GridSettings) {
    //check settings and set default
    for(let c of settings.columns) {
      if(c.visible == null) {
        c.visible = true;
      }
      if(c.canEdit == null) {
        c.canEdit = true;
      }
      if(c.type == null) {
        c.type = 'string';
      }
      if(c.caption == null) {
        c.caption = c.field;
      }
      if(c.validationRules != null) {
        for(let rule of c.validationRules) {
          if(rule.type === 'unique') {
            rule.type = 'custom';
            rule.validationCallback = (e) => {
              for (let r of this.dataSource) {
                if(r[c.field] === e.value && r[settings.key] !== e['validator']['_validationGroup']['key']) {
                  return false;
                }
              }
              return true;
            };
            rule.message = 'Field must be unique';
          }
        }
      }
    }

    this.settings = settings;
    this.load();
  }

  public constructor(private authenticationService: AuthenticationService) {
    this.dataSource = [];
    this.isLoading = true;
  }


  load() {
    this.dataSource.length = 0;
    this.authenticationService.get<any[]>(`api/${this.settings.name}`)
      .subscribe((r) => {
        if (r) {
          for(let i of r) {
            this.dataSource.push(i);
          }
        }
        this.isLoading = false;
      });
  }

  rowAdded(e) {
    this.upsertData(e.data);
  }

  rowUpdated(e) {
    this.upsertData(this.dataSource.filter(v => v[this.settings.key] === e.key)[0]);
  }

  rowRemoved(e) {
    this.authenticationService.delete<any>(`api/${this.settings.name}/${e.key}`)
      .subscribe((data) => {
        if(!data) {
          this.load();
        }
      });
  }

  upsertData(data: any) {
    this.authenticationService.post<any>(`api/${this.settings.name}/Upsert`, data)
      .subscribe((data) => {
        if (data) {
          this.dataSource.filter(v => v[this.settings.key] === data[this.settings.key])[0] = data;
        } else {
          this.load();
        }
      });
  }

}

export class GridSettings {
  public name: string;
  public key: string;
  public columns: Array<GridColumn>;
}

export class GridColumn {
  public field: string;
  public caption: string;
  public type: string;
  public visible: boolean;
  public canEdit: boolean;
  public lookupSettings: LookupSettings;
  public validationRules: Array<ValidationRule>;
}


export class LookupSettings {
  public dataSource: any;
  public valueExpr: string;
  public displayExpr: string;
}

export class ValidationRule {
  public type: string;
  public pattern: any;
  public message: string;
  public min: any;
  public max: any;
  public validationCallback: any;
}

import { AuthenticationService } from '../user-module/services/authentication.service';

export abstract class BaseComplexPickListService {

  protected constructor(protected authenticationService: AuthenticationService, odataController: string) {
    this.source = this.authenticationService.getOdataStoreWithFilter(
      'odata/' + odataController, null);
  }

  protected source: any;

  public getSource(): any  {
    return this.source;
  }

  public getPickListEditorOptions(includeInActive?: boolean): any {

    if(!includeInActive) {
      this.source.filter = ['active', '=', true];
    }

    let name = this.authenticationService.language + 'Value';

    this.source.sort = [
      { selector: name, desc: false }
    ];

    return { dataSource: this.source,
      displayExpr: name,
      valueExpr:'id',
      searchEnabled: true
    };
  }

  public getPickListEditorOptionsByKey(includeInActive?: boolean): any {

    if(!includeInActive) {
      this.source.filter = ['active', '=', true];
    }

    let name = this.authenticationService.language + 'Value';

    this.source.sort = [
      { selector: name, desc: false }
    ];

    return { dataSource: this.source,
      displayExpr: name,
      valueExpr:'key',
      searchEnabled: true
    };
  }

}

import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-protected',
  templateUrl: './protected.component.html',
  styleUrls: ['./protected.component.css']
})
export class ProtectedComponent {

  innerHeight: number = 500;

  year = new Date().getFullYear();
  path = '';

  constructor(router: Router) {

    this.innerHeight = window.innerHeight;

    router.events.subscribe((val) => {
      const url = val['url'];
      if(url != null && url.length > 0) {
        const res = url.split('/');
        this.path = res[res.lenght-1];
      } else {
        this.path = '';
      }

    });
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerHeight = window.innerHeight;
  }

}

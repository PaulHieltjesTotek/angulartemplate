import { Injectable } from '@angular/core';
import { AuthenticationService } from '../../user-module/services/authentication.service';
import { PickListResult } from '../models/pick-list-result';
import { Observable } from 'rxjs';

@Injectable()
export class PickListService {

  constructor(private authenticationService: AuthenticationService) { }

  public getPickListValues(type: string, includeInActive?: boolean): Observable<Array<PickListResult>> {
    if(includeInActive == null) {
      includeInActive = false;
    }

    return this.authenticationService.get<PickListResult[]>(`api/PickList/GetValuesForType/${type}/${includeInActive}`);
  }

  public getPickLists(types: Array<string>, includeInActive?: boolean): Observable<any>
  {
    let values = [];
    for(let t of types) {
      values.push(new Observable<any>((obs) => {
        this.getPickListValues(t, includeInActive).subscribe(r => {
          obs.next({
            type: t,
            values: r
          });
          obs.complete();
          obs.unsubscribe();
        });
      }));
    }

    return new Observable<any>(
      (obs) => {
        Observable.forkJoin(values).subscribe(
        (result: Array<any>) => {
          let r = {};

          for(let t of types) {
            r[t] = result.filter(a => a.type === t)[0].values;
          }

          obs.next(r);
          obs.complete();
          obs.unsubscribe();
        });
      }
    );

  }

  public setPickListValues(values: PickListResult[], type: string, includeInActive?: boolean): void {

    this.getPickListValues(type, includeInActive)
      .subscribe(r => {
        if (r) {
          for(let i of r) {
            values.push(i);
          }
        }
      });
  }

  public getPickListEditorOptions(type: string, includeInActive?: boolean): any {
    let values = [];

    let result = { dataSource: values,
      loaded: false,
      displayExpr:'value',
      valueExpr:'id',
      searchEnabled: true,
      getKey: (id) => {
        let value = values.filter(f => f.id === id);
        return value.length > 0 ? value[0].key : '';
      }, getByKey: (key) => {
        let value = values.filter(f => f.key === key);
        return value.length > 0 ? value[0].id : '';
      }
    };

    this.getPickListValues(type, includeInActive)
      .subscribe(r => {
        if (r) {
         for(let i of r) {
           values.push(i);
         }
          result.loaded = true;
        } else {
          result.loaded = true;
        }
      });

    return result
  }

}

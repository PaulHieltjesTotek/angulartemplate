export class PickList {
  public id: any;
  public type: string;
  public key: string;
  public enValue: string;
  public nlValue: string;
  public frValue: string;
  public active: boolean;
}

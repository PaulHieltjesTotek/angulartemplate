export class PickListResult {
  public id: any;
  public type: string;
  public key: string;
  public value: string;
  public active: boolean;
}

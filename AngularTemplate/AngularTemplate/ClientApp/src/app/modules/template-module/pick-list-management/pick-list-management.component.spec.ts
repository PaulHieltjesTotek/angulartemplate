import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickListManagementComponent } from './pick-list-management.component';

describe('PickListManagementComponent', () => {
  let component: PickListManagementComponent;
  let fixture: ComponentFixture<PickListManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickListManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickListManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../user-module/services/authentication.service';
import { GridLayout } from '../../custom-grid-module/models/grid-layout';
import { PickList } from '../models/pick-list';

@Component({
  selector: 'app-pick-list-management',
  templateUrl: './pick-list-management.component.html',
  styleUrls: ['./pick-list-management.component.css']
})
export class PickListManagementComponent implements OnInit {

  picklistTypes: Array<string>;
  picklistValues: Array<PickList>;
  private currentType: string;

  constructor(private authenticationService: AuthenticationService) {
    this.picklistTypes = [];
    this.picklistValues = [];
  }

  ngOnInit() {
    this.authenticationService.get<string[]>('api/PickList/GetPickListTypes')
      .subscribe((r) => {
        if (r) {
          for(let i of r) {
            this.picklistTypes.push(i);
          }
        }
      });
  }

  public changePicklist(e): void {
    this.currentType = e.value;
    this.picklistValues.length = 0;
    this.authenticationService.get<PickList[]>('api/PickList/GetRawValuesForType/' + e.value)
      .subscribe((r) => {
        if (r) {
          for(let i of r) {
            this.picklistValues.push(i);
          }
        }
      });
  }

  rowAdded(e) {
    debugger;
    let data = e.data;
    data.type = this.currentType;
    this.upsertData(data);
  }

  rowUpdated(e) {
    this.upsertData(this.picklistValues.filter(v => v.id === e.key)[0]);
  }

  upsertData(data: PickList) {
    debugger;
    this.authenticationService.post<PickList>('api/PickList/Upsert', data)
      .subscribe((data) => {
        if (data) {
          this.changePicklist({value: this.currentType});
        }
      });
  }

}

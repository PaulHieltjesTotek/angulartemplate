import { Component, HostListener } from '@angular/core';
import { AuthenticationService } from '../../user-module/services/authentication.service';
import { UserRolesConstants } from '../../user-module/user-roles';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent {

  activeDropdown = '';
  showNavbarCollapse = false;
  UserRolesConstants = UserRolesConstants;

  constructor(private authenticationService: AuthenticationService) { }

  public setActiveDrowdown(event: any, value: string): void {
    event.stopPropagation();

    if(this.activeDropdown === value) {
      this.activeDropdown = '';
    } else {
      this.activeDropdown = value;
    }
  }

  public hasRole(role: string): boolean {
    return this.authenticationService.userHasRole(role);
  }

  public hasOneOfRoles(roles: Array<any>): boolean {

    for (let r of roles) {
      if (r instanceof Array) {
        for (let s of r) {
          if (this.authenticationService.userHasRole(s)) {
            return true;
          }
        }
      } else {
        if (this.authenticationService.userHasRole(r)) {
          return true;
        }
      }
    }

    return false;
  }


  public toggleNavbarCollapse(): void {
    this.showNavbarCollapse = !this.showNavbarCollapse;
  }

  @HostListener('document:click')
  clickout() {
    this.activeDropdown = '';
  }

}

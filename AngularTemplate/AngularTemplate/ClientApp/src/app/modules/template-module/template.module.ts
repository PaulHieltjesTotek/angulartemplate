import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProtectedComponent } from "./protected/protected.component";
import { HeaderComponent } from './header/header.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { UserModule } from '../user-module/user.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { PickListManagementComponent } from './pick-list-management/pick-list-management.component';
import { AuthGuard } from '../user-module/guard/auth-guard';
import { RoleGuard } from '../user-module/guard/role-guard';
import { DxDataGridModule, DxSelectBoxModule } from 'devextreme-angular';
import { LoginGuard } from '../user-module/guard/login-guard';
import { PickListService } from './services/pick-list.service';
import { UserRolesConstants } from '../user-module/user-roles';


@NgModule({
  imports: [
    CommonModule,
    UserModule,
    HttpClientModule,
    DxSelectBoxModule,
    DxDataGridModule,
    RouterModule.forRoot([
      { path: 'Protected', component: ProtectedComponent, canActivate: [AuthGuard],
        children: [
          { path: 'PickListManagement', component: PickListManagementComponent, canActivate: [RoleGuard], data: { roles: [UserRolesConstants.adminRole]} },
        ]
      }
    ])
  ],
  declarations: [
    HeaderComponent,
    NavBarComponent,
    ProtectedComponent,
    PickListManagementComponent,
  ], exports: [
    ProtectedComponent,
  ],
  providers: [
    PickListService,
    LoginGuard,
    AuthGuard,
    RoleGuard
  ],
})
export class TemplateModule { }

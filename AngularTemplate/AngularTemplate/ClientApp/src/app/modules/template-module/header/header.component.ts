import { Component, HostListener } from '@angular/core';
import { AuthenticationService } from '../../user-module/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  menuOpen = false;
  languageOpen = false;

  get language(): string {
    switch(this.authenticationService.language)
    {
      case 'nl':
        return 'Nederlands';
      case 'fr':
        return 'Français';
      default:
        return 'English';
    }

  }

  public get username(): string {
    return this.authenticationService.getLoggedInUserName();
  }

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  public toggleMenu(event: any) {
    event.stopPropagation();

    this.menuOpen = !this.menuOpen;
  }

  public toggleLanguage(event: any) {
    event.stopPropagation();

    this.languageOpen = !this.languageOpen;
  }

  @HostListener('document:click')
  clickout() {
    this.menuOpen = false;
    this.languageOpen = false;
  }

  changeLanguage(language: string) {
    this.authenticationService.language = language;
  }

  changePassword() {
    this.router.navigate(['/ChangePassword']);
  }

  logout() {
    this.authenticationService.logout();
  }

}

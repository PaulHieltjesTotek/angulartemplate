import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../user-module/services/authentication.service';
import { CustomOperatorHelper } from '../../../helpers/custom-operator-helper';
import { EdmLiteral } from 'devextreme/data/odata/utils';

@Component({
  selector: 'app-workflow-history',
  templateUrl: './workflow-history.component.html',
  styleUrls: ['./workflow-history.component.css']
})
export class WorkflowHistoryComponent {

  popupPosition: any;
  customOperations: Array<any>;
  dataSource: any;
  historyDataSourceStorage: any;
  display: string;
  statusSource: any;

  constructor(private authenticationService: AuthenticationService) {
    this.dataSource = this.authenticationService.getOdataStore('odata/ODataWorkflow');
    this.popupPosition = { my: 'top', at: 'top', of: window };
    this.customOperations = CustomOperatorHelper.getCustomOperators();
    this.historyDataSourceStorage = [];
    this.display = this.authenticationService.language + 'Value';
  }

  getHistory(id: any) {
      let item = this.historyDataSourceStorage.find((i) => i.key === id);
      if (!item) {
        item = {
          key: id,
          dataSourceInstance: this.authenticationService.getOdataStoreWithFilter(
            'odata/ODataWorkflowHistory', ['workflowId', '=', new EdmLiteral(id)])
        };
        this.historyDataSourceStorage.push(item);

      }

    return item.dataSourceInstance;
  }

}

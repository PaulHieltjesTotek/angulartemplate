import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkflowMessageModalComponent } from './workflow-message-modal.component';

describe('QuestionsModalComponent', () => {
  let component: WorkflowMessageModalComponent;
  let fixture: ComponentFixture<WorkflowMessageModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkflowMessageModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkflowMessageModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { WfMessageResult } from '../models/wf-message-result';
import { AuthenticationService } from '../../user-module/services/authentication.service';

@Component({
  selector: 'app-workflow-message-modal',
  templateUrl: './workflow-message-modal.component.html',
  styleUrls: ['./workflow-message-modal.component.css']
})
export class WorkflowMessageModalComponent {

  showMessage: boolean;
  message: string;
  language: string;

  private observer: Subscriber<WfMessageResult>;
  constructor(private authenticationService: AuthenticationService) {
    this.showMessage = false;
  }

  public showDialog(languageKey?: string): Observable<WfMessageResult> {

    if(languageKey == null) {
      this.language = 'LANG_EN';
    } else {
      this.language = languageKey;
    }

    this.showMessage = true;
    this.message = '';
    return new Observable<WfMessageResult>((observer) => {
      this.observer = observer;
    });
  }

  public insertDefaultText() {
    switch (this.language) {
      case "LANG_NL":
        this.message = `Op basis van de ontvangen informatie kunnen we de behandeling van uw kaartaanvraag helaas niet verderzetten.
        Redenen hiervoor kunnen de volgende zijn:
        - Ons huidige krediet- en risicobeleid: elke aanvraag ondergaat een automatisch kredietanalyseproces, ontwikkeld op basis van verantwoord lenen en in overeenstemming met de commerciële strategie van ons bedrijf.
      - De verstrekte informatie en documentatie was niet voldoende om ons in staat te stellen een volledige analyse van uw aanvraag uit te voeren.
      - De resultaten van de opzoeking uitgevoerd door onze dienst gebaseerd op de ontvangen informatie. `;
        break;
      case "LANG_FR":
        this.message = `Sur base des informations reçues, nous ne sommes malheureusement pas en mesure de procéder avec votre demande.
        Les raisons peuvent être les suivantes:
        - Notre politique actuelle en matière de crédit et de risque: chaque demande est soumise à un processus automatique d'évaluation du crédit, développé sur le principe du crédit responsable et conforme à la stratégie commerciale de notre société.
      - Les informations et la documentation fournies n’ont pas été suffisantes pour nous permettre de procéder à une analyse complète de votre demande.
      - Les résultats des recherches effectuées par nos services sur base des informations reçues.`;
        break;
      default:
        this.message = `Based on the information received, we are unfortunately not able to proceed with your application. 
        Reasons for this may include the following:
        - Our current Credit and Risk policy: each application undergoes an automatic credit scoring process, developed on the principle of responsable lending and in accordance with our company's commercial strategy.
      - The information and documentation provided is insufficient to allow us to conduct a complete analysis of your application.
      - The results of the searches carried out by our services based on the information received.`;
        break;
    }
  }

  public closeModal(isSuccesful: boolean) {
    this.observer.next({ isSuccesful: isSuccesful, message: this.message});
    this.showMessage = false;
    this.observer.complete();
    this.observer.unsubscribe();
  }

}

import { WorkflowVariableResultBase } from '../models/wf-message-result';
import { AuthenticationService } from '../../user-module/services/authentication.service';

export class WfHelper {

  public static canUpdate(model: WorkflowVariableResultBase, authenticationService: AuthenticationService): boolean {
    if (model.isAssigneeUser)
    {
      return model.assignee.toLowerCase() === authenticationService.getLoggedInUserName().toLowerCase();
    }

    return authenticationService.userHasRole(model.assignee);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkflowHistoryComponent } from './workflow-history/workflow-history.component';
import { RouterModule } from '@angular/router';
import { ProtectedComponent } from '../template-module/protected/protected.component';
import { AuthGuard } from '../user-module/guard/auth-guard';
import { RoleGuard } from '../user-module/guard/role-guard';
import {
  DxButtonModule,
  DxDataGridModule,
  DxFormModule,
  DxLoadPanelModule,
  DxPopupModule,
  DxTextAreaModule
} from 'devextreme-angular';
import { CustomGridModule } from '../custom-grid-module/custom-grid.module';
import { WorkflowMessageModalComponent } from './workflow-message-modal/workflow-message-modal.component';
import { UserRolesConstants } from '../user-module/user-roles';

@NgModule({
  imports: [
    CommonModule,
    CustomGridModule,
    DxDataGridModule,
    DxFormModule,
    DxPopupModule,
    DxTextAreaModule,
    DxLoadPanelModule,
    DxButtonModule,
    RouterModule.forRoot([
      { path: 'Protected', component: ProtectedComponent, canActivate: [AuthGuard],
        children: [
          { path: 'WorkflowHistory', component: WorkflowHistoryComponent, canActivate: [RoleGuard], data: { roles: [UserRolesConstants.workflowAdmin]} },
        ]
      }
    ])
  ],
  declarations: [WorkflowHistoryComponent, WorkflowMessageModalComponent],
  exports: [
    WorkflowMessageModalComponent
  ]
})
export class WorkflowModule { }

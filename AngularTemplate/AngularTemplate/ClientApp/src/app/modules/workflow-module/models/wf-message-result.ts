export class WfMessageResult {
  public isSuccesful: boolean;
  public message: string;
}


export class WorkflowVariableBase implements IWorkflowVariableBase {
  message?: string | undefined;

  constructor(data?: IWorkflowVariableBase) {
    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property))
          (<any>this)[property] = (<any>data)[property];
      }
    }
  }

  init(data?: any) {
    if (data) {
      this.message = data["message"];
    }
  }

  static fromJS(data: any): WorkflowVariableBase {
    data = typeof data === 'object' ? data : {};
    let result = new WorkflowVariableBase();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["message"] = this.message;
    return data;
  }
}

export interface IWorkflowVariableBase {
  message?: string | undefined;
}

export class ApproveViewModel extends WorkflowVariableBase implements IApproveViewModel {
  id: string;

  constructor(data?: IApproveViewModel) {
    super(data);
  }

  init(data?: any) {
    super.init(data);
    if (data) {
      this.id = data["id"];
    }
  }

  static fromJS(data: any): ApproveViewModel {
    data = typeof data === 'object' ? data : {};
    let result = new ApproveViewModel();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    super.toJSON(data);
    return data;
  }
}

export interface IApproveViewModel extends IWorkflowVariableBase {
  id: string;
}

export class WorkflowVariableResultBase extends WorkflowVariableBase implements IWorkflowVariableResultBase {
  id?: string | undefined;
  triggerNames?: string[] | undefined;
  state?: string | undefined;
  assignee?: string | undefined;
  isAssigneeUser: boolean;

  constructor(data?: IWorkflowVariableResultBase) {
    super(data);
  }

  init(data?: any) {
    super.init(data);
    if (data) {
      this.id = data["id"];
      if (Array.isArray(data["triggerNames"])) {
        this.triggerNames = [] as any;
        for (let item of data["triggerNames"])
          this.triggerNames.push(item);
      }
      this.state = data["state"];
      this.assignee = data["assignee"];
      this.isAssigneeUser = data["isAssigneeUser"];
    }
  }

  static fromJS(data: any): WorkflowVariableResultBase {
    data = typeof data === 'object' ? data : {};
    let result = new WorkflowVariableResultBase();
    result.init(data);
    return result;
  }

  toJSON(data?: any) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    if (Array.isArray(this.triggerNames)) {
      data["triggerNames"] = [];
      for (let item of this.triggerNames)
        data["triggerNames"].push(item);
    }
    data["state"] = this.state;
    data["assignee"] = this.assignee;
    data["isAssigneeUser"] = this.isAssigneeUser;
    super.toJSON(data);
    return data;
  }
}

export interface IWorkflowVariableResultBase extends IWorkflowVariableBase {
  id?: string | undefined;
  triggerNames?: string[] | undefined;
  state?: string | undefined;
  assignee?: string | undefined;
  isAssigneeUser: boolean;
}

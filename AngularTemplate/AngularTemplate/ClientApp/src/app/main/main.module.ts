import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { ProtectedComponent } from '../modules/template-module/protected/protected.component';
import { AuthGuard } from '../modules/user-module/guard/auth-guard';
import { DxDataGridModule } from 'devextreme-angular';
import { UserModule } from '../modules/user-module/user.module';
import { CustomGridModule } from '../modules/custom-grid-module/custom-grid.module';

@NgModule({
  imports: [
    CommonModule,
    UserModule,
    CustomGridModule,
    DxDataGridModule,
    RouterModule.forRoot([
      { path: 'Protected', component: ProtectedComponent, canActivate: [AuthGuard],
        children: [
          { path: 'Dashboard', component: HomeComponent },
        ]
      }
    ])
  ],
  declarations: [
    HomeComponent
  ]
})
export class MainModule { }

import { Component, Inject } from '@angular/core';
import { CustomOperatorHelper } from '../../helpers/custom-operator-helper';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {

  dataSource: any;
  popupPosition: any;
  customOperations: Array<any>;

  constructor(@Inject('BASE_URL') private baseUrl: string) {
    this.init();
  }

  private init() {
    this.dataSource = [{
      id: 1,
      name: 'Tjitte',
      lastName: 'de Visscher',
      age: 29
    }, {
      id: 2,
      name: 'Niek',
      lastName: 'de Visscher',
      age: 35
    }, {
      id: 3,
      name: 'Paul',
      lastName: 'Hieltjes',
      age: 29
    }, {
      id: 4,
      name: 'Salvi',
      lastName: 'Jansen',
      age: 31
    }, {
      id: 5,
      name: 'Jeroen',
      lastName: 'Habils',
      age: 32
    }];
    this.popupPosition = { my: 'top', at: 'top', of: window };
    this.customOperations = CustomOperatorHelper.getCustomOperators();
  }
}

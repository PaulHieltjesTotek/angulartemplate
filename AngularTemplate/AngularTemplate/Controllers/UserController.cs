using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Attributes;
using Core.Models;
using Domain;
using Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared.ViewModels;

namespace AngularTemplate.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
	    private readonly ILogger<UserController> _logger;

		public UserController(IUserService userService, ILogger<UserController> logger)
        {
            _userService = userService;
	        _logger = logger;

        }

        [HttpGet("[action]/{username}"), AllowAnonymous]
        public async Task<ReturnModel<bool>> ResetPassword(string username)
        {
            return await _userService.ResetPassword(username);
        }
        
        [HttpPost("[action]")]
        public async Task<ReturnModel<bool>> ChangePassword([FromBody] ChangePasswordViewModel model)
        {
            return await _userService.ChangePassword(model);
        }
        
        [HttpPost("[action]")]
        public async Task<ReturnModel<Guid>> Register([FromBody]RegistrationViewModel model)
        {
            return !ModelState.IsValid 
                ? ReturnModel<Guid>.ReturnError(ModelState) 
                : await _userService.RegisterUser(model);
        }
        
        [HttpPost("[action]"), AllowAnonymous]
        public async Task<ReturnModel<string>> Login([FromBody]CredentialsViewModel model)
        {
	        _logger.Log(LogLevel.Information, "User logged in!");

			return !ModelState.IsValid 
                ? ReturnModel<string>.ReturnError(ModelState) 
                : await _userService.Login(model);
        }
        
        [HttpGet("[action]/{userId:Guid}"), Authorize(UserRoles.AdminRole)]
        public async Task<ReturnModel<IEnumerable<string>>> GetRolesForUser(Guid userId)
        {
            return await _userService.GetRolesForUser(userId);
        }
        
        [HttpGet("[action]/{userId:Guid}"), Authorize(UserRoles.AdminRole)]
        public async Task<ReturnModel<IEnumerable<string>>> GetMissingRolesForUser(Guid userId)
        {
            return await _userService.GetMissingRolesForUser(userId);
        }
        
        [HttpGet("[action]/{userId:Guid}/{role}"), Authorize(UserRoles.AdminRole)]
        public async Task<ReturnModel<bool>> RemoveRoleFromUser(Guid userId, string role)
        {
            return await _userService.RemoveRoleFromUser(userId, role);
        }
        
        [HttpGet("[action]/{userId:Guid}/{role}"), Authorize(UserRoles.AdminRole)]
        public async Task<ReturnModel<bool>> AddRoleToUser(Guid userId, string role)
        {
            return await _userService.AddRoleToUser(userId, role);
        }
     
        [Roles(UserRoles.AdminRole)]
        [HttpPost("[action]")]
        public async Task<ReturnModel<bool>> CreateUser([FromBody] UserCreationViewModel user)
        {
            return await _userService.CreateUser(user);
        }

        [Authorize(UserRoles.AdminRole)]
        [HttpGet("[action]/{id:Guid}/{active:bool}")]
        public async Task<ReturnModel<bool>> SetActivenessUser(Guid id, bool active)
        {
            return await _userService.SetActivenessUser(id, active);
        }

        [Authorize(UserRoles.AdminRole)]
        [HttpDelete("[action]/{id:Guid}")]
        public async Task<ReturnModel<bool>> DeleteUser(Guid id)
        {
            return await _userService.DeleteUser(id);
        }
        
        
//        [Authorize(UserRoles.AirPlusBo)]
//        [HttpGet("[action]/{id:Guid}")]
//        public async Task<ReturnModel<bool>> ActivateUser(Guid id)
//        {
//            return await _userService.ActivateUser(id);
//        }
    }
}
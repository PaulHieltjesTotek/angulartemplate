using System;
using System.Linq;
using Core.Attributes;
using DbContext;
using Domain;
using Microsoft.AspNet.OData;
using Shared.Models.Authentication;

namespace AngularTemplate.Controllers
{

    [Roles(UserRoles.AdminRole)]
    public class ODataUserController: ODataController
    {
        private readonly IdentityContext _context;
    
        public ODataUserController(IdentityContext context)
        {
            _context = context;
        }
    
        [EnableQuery(MaxNodeCount = 100000)]
        public IQueryable<AppUser> Get() => _context.AppUsers.AsQueryable();
    
        public AppUser GetAppUser([FromODataUri] Guid key)
        {
            var k = key.ToString();
            return _context.AppUsers.Single(b => b.Id == k);
        }
    }
}
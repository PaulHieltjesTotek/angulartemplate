using System;
using System.Linq;
using DbContext;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using tomware.Microwf.AspNetCoreEngine.Common;
using tomware.Microwf.AspNetCoreEngine.Domain;

namespace AngularTemplate.Controllers
{
    [Authorize(Constants.MANAGE_WORKFLOWS_POLICY)]
    public class ODataWorkflowController: ODataController
    {
        private readonly WorkFlowContext _context;
        
        public ODataWorkflowController(WorkFlowContext context)
        {
            _context = context;
        }
        
        [EnableQuery(MaxNodeCount = 100000)]
        public IQueryable<Workflow> Get() => _context.Workflows.AsQueryable();
        
        public Workflow GetWorkflow([FromODataUri] Guid key)
        {
            return _context.Workflows.Single(b => b.Id == key);
        }
    }
}
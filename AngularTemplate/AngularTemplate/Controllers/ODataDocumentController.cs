using System;
using System.Linq;
using DbContext;
using Domain.Models.Application;
using Microsoft.AspNet.OData;

namespace AngularTemplate.Controllers
{
    
    public class ODataDocumentController: ODataController
    {
        private readonly ApplicationContext _context;
    
        public ODataDocumentController(ApplicationContext context)
        {
            _context = context;
        }
    
        [EnableQuery(MaxNodeCount = 100000)]
        public IQueryable<Document> Get() => _context.Documents.AsQueryable();
    
        public Document GetDocument([FromODataUri] Guid key)
        {
            return _context.Documents.Single(b => b.Id == key);
        }
    }
}
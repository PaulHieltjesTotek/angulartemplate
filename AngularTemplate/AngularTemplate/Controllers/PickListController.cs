using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;
using Domain;
using Domain.Models.Application;
using Infrastructure.Base;
using Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AngularTemplate.Controllers
{
    [Route("api/[controller]"), Authorize(UserRoles.AdminRole)]
    public class PickListController : BaseRestController<PickList>
    {
        private readonly IPickListService _service;
        
        public PickListController(IPickListService service) : base(service)
        {
            _service = service;
        }

        [AllowAnonymous]
        [HttpGet("[action]/{type}")]
        [HttpGet("[action]/{type}/{includeInActive:bool}")]
        public async Task<ReturnModel<IEnumerable<PickListViewModel>>> GetValuesForType(string type,
            bool includeInActive = true)
        {
            return await _service.GetValuesForType(type, includeInActive);
        }

        [HttpGet("[action]/{type}")]
        public async Task<ReturnModel<IEnumerable<PickList>>> GetRawValuesForType(string type)
        {
            return await _service.GetRawValuesForType(type);
        }

        [HttpGet("[action]")]
        public async Task<ReturnModel<IEnumerable<string>>> GetPickListTypes()
        {
            return await _service.GetPickListTypes();
        }
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;
using Domain.Models.Application;
using Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace AngularTemplate.Controllers
{
    [Route("api/[controller]")]
    public class GridLayoutController : Controller
    {
        private readonly IGridLayoutService _gridLayoutService;

        public GridLayoutController(IGridLayoutService service)
        {
            _gridLayoutService = service;
        }
        
        [HttpGet("{gridName}")]
        public ReturnModel<IEnumerable<GridLayout>> Get(string gridName)
        {
            return _gridLayoutService.GetAllForGrid(gridName);
        }
        
        [HttpPost]
        public async Task<ReturnModel<GridLayout>> Post([FromBody] GridLayout model)
        {
            return await _gridLayoutService.Post(model);
        }
        
        [HttpPut]
        public async Task<ReturnModel<GridLayout>> Put([FromBody] GridLayout model)
        {
            return await _gridLayoutService.Put(model);
        }
        
        [HttpDelete("{id}")]
        public ReturnModel<string> Delete(Guid id)
        {
            return _gridLayoutService.Delete(id);
        }
    }
}
using System;
using System.Linq;
using Core.Attributes;
using DbContext;
using Domain;
using Microsoft.AspNet.OData;
using tomware.Microwf.AspNetCoreEngine.Domain;

namespace AngularTemplate.Controllers
{
    [Roles(UserRoles.AdminRole, UserRoles.WorkflowAdmin)]
    public class ODataWorkflowHistoryController: ODataController
    {
        private readonly WorkFlowContext _context;
        
        public ODataWorkflowHistoryController(WorkFlowContext context)
        {
            _context = context;
        }
        
        [EnableQuery(MaxNodeCount = 100000)]
        public IQueryable<WorkflowHistory> Get() => _context.WorkflowHistories.AsQueryable();
     
        public WorkflowHistory GetWorkflowHistory([FromODataUri] Guid key)
        {
            return _context.WorkflowHistories.Single(b => b.Id == key);
        }
    }
}
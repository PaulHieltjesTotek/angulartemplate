using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Shared.Models.Authentication
{
    public class AppUser : IdentityUser
    {
        // Extended Properties
        [MaxLength(256)]
        public string LandingPage { get; set; }

        public bool Active { get; set; }
    }

}
using FluentValidation.Attributes;
using Shared.ViewModels.Validations;

namespace Shared.ViewModels
{
    [Validator(typeof(RegistrationViewModelValidator))]
    public class RegistrationViewModel 
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public string Location { get; set; }      
        public string LandingPage { get; set; }  
        
        public bool Active { get; set; }
    }
}
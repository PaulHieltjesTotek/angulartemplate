using FluentValidation.Attributes;
using Shared.ViewModels.Validations;

namespace Shared.ViewModels
{
    [Validator(typeof(CredentialsViewModelValidator))]
    public class CredentialsViewModel  
    {
        public string UserName { get; set; }
        public string Password { get; set; }        
    }

    public class ChangePasswordViewModel  
    {
        public string CurrentPassword { get; set; }        
        public string NewPassword { get; set; }   
        public string ConfirmPassword { get; set; } 
    }
}
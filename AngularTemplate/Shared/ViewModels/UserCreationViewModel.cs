namespace Shared.ViewModels
{
    public class UserCreationViewModel
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string InitialRole { get; set; }
        public bool Activate { get; set; }
    }
}
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Attributes;
using DbContext;
using Domain.Models;
using Domain.Models.Application;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Persistence.Base;
using Persistence.Repositories.Interfaces;

namespace Persistence.Repositories
{
    [AutoDi(AddType.Scoped, typeof(IPickListRepository))]
    public class PickListRepository : BaseRepository<PickList, ApplicationContext>, IPickListRepository
    {
        public PickListRepository(IServiceScopeFactory contextFactory, ILogger<BaseRepository<PickList, ApplicationContext>> logger) 
            : base(contextFactory, logger)
        {
        }

        public async Task<IEnumerable<PickList>> GetValuesForType(string type, bool includeInActive)
        {
            using (var context = GetContext())
            {
                return includeInActive
                    ? await context.PickLists.Where(p => p.Type == type).ToListAsync()
                    : await context.PickLists.Where(p => p.Type == type && p.Active).ToListAsync();
            }
            
        }
        
        public async Task<IEnumerable<string>> GetPicklistTypes()
        {
            using (var context = GetContext())
            {
                return await context.PickLists.Select(p => p.Type).Distinct().ToListAsync();
            }
            
        }
        
        public async Task<PickList> GetPicklistByKey(string key)
        {
            using (var context = GetContext())
            {
                return await context.PickLists.FirstOrDefaultAsync(p => p.Key == key);
            }
            
        }
        
        
    }
}
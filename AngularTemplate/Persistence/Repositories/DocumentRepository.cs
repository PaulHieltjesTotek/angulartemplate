using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Attributes;
using DbContext;
using Domain.Models;
using Domain.Models.Application;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Persistence.Base;
using Persistence.Repositories.Interfaces;

namespace Persistence.Repositories
{
    [AutoDi(AddType.Scoped, typeof(IDocumentRepository))]
    public class DocumentRepository : BaseRepository<Document, DatabaseContext>, IDocumentRepository
    {
        public DocumentRepository(IServiceScopeFactory contextFactory, ILogger<BaseRepository<Document, DatabaseContext>> logger) 
            : base(contextFactory, logger)
        {
        }
        
    }
}
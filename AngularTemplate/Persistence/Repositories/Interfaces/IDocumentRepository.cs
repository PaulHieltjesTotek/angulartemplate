using Domain.Models;
using Domain.Models.Application;
using Persistence.Base;

namespace Persistence.Repositories.Interfaces
{
    public interface IDocumentRepository : IBaseRepository<Document>
    {
    }
}
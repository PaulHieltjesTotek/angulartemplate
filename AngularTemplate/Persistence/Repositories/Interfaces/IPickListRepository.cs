using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;
using Domain.Models.Application;
using Persistence.Base;

namespace Persistence.Repositories.Interfaces
{
    public interface IPickListRepository : IBaseRepository<PickList>
    {
        Task<IEnumerable<PickList>> GetValuesForType(string type, bool includeInActive);
        Task<IEnumerable<string>> GetPicklistTypes();
        Task<PickList> GetPicklistByKey(string key);
    }
}
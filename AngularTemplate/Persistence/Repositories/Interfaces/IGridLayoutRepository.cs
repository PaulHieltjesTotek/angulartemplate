using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models.Application;

namespace Persistence.Repositories.Interfaces
{
    public interface IGridLayoutRepository
    {
        Task<GridLayout> GetAsync(Guid id);

        GridLayout Get(Guid id);

        Task<IEnumerable<GridLayout>> GetAllForGridAsync(string gridId, Guid userId);

        IEnumerable<GridLayout> GetAllForGrid(string gridId, Guid userId);

        Task<IEnumerable<GridLayout>> GetAllAsync();

        IEnumerable<GridLayout> GetAll();

        void Delete(Guid id);

       Task<GridLayout> PostAsync(GridLayout record);

       GridLayout Post(GridLayout record);

       Task<GridLayout> PutAsync(GridLayout record);

       GridLayout Put(GridLayout record);

    }
}
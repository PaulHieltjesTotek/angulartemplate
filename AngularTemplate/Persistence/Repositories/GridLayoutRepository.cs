using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Attributes;
using DbContext;
using Domain.Models.Application;
using Microsoft.EntityFrameworkCore;
using Persistence.Repositories.Interfaces;

namespace Persistence.Repositories
{
    [AutoDi(AddType.Scoped, typeof(IGridLayoutRepository))]
    public class GridLayoutRepository : IGridLayoutRepository
    {
        private readonly ApplicationContext _context;

        public GridLayoutRepository(ApplicationContext context)
        {
            _context = context;
        }

        public async Task<GridLayout> GetAsync(Guid id)
        {
            return await _context.GridLayouts.FirstOrDefaultAsync(l => l.Id == id);
        }

        public GridLayout Get(Guid id)
        {
            return _context.GridLayouts.FirstOrDefault(l => l.Id == id);
        }
        
        public async Task<IEnumerable<GridLayout>> GetAllForGridAsync(string gridId, Guid userId)
        {
            var result = await _context.GridLayouts
                .Where(l => l.GridId == gridId && (l.UserId == userId || l.UserId == null))
                .OrderBy(u => u.UserId)
                .ToListAsync();
            
            result.Add(new GridLayout
            {
                GridId = gridId,
                LayoutName = "Default",
                Layout = "{}"
            });

            return result;
        }

        public IEnumerable<GridLayout> GetAllForGrid(string gridId, Guid userId)
        {
            var result = _context.GridLayouts
                .Where(l => l.GridId == gridId && (l.UserId == userId || l.UserId == null))
                .OrderBy(u => u.UserId)
                .ToList();
            
            result.Add(new GridLayout
            {
                GridId = gridId,
                LayoutName = "Default",
                Layout = "{}"
            });

            return result;
        }
        
        public async Task<IEnumerable<GridLayout>> GetAllAsync()
        {
            return await _context.GridLayouts.ToListAsync();
        }

        public IEnumerable<GridLayout> GetAll()
        {
            return _context.GridLayouts.ToList();
        }
        
        public async Task DeleteAsync(Guid id)
        {
            var entity = await _context.GridLayouts.FirstOrDefaultAsync(m => m.Id.Equals(id));

            if (entity != null)
            {
                _context.GridLayouts.Remove(entity);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new NullReferenceException($"No record found with id: {id.ToString()}");
            }
        }

        public void Delete(Guid id)
        {
            var entity = _context.GridLayouts.FirstOrDefault(m => m.Id.Equals(id));

            if (entity != null)
            {
                _context.GridLayouts.Remove(entity);
                _context.SaveChanges();
            }
            else
            {
                throw new NullReferenceException($"No record found with id: {id.ToString()}");
            }
        }
        
        public async Task<GridLayout> PostAsync(GridLayout record)
        {
            var entity = await _context.GridLayouts.AddAsync(record);
            await _context.SaveChangesAsync();
            return entity.Entity;
        }

        public GridLayout Post(GridLayout record)
        {
            var entity = _context.GridLayouts.Add(record);
            _context.SaveChanges();
            return entity.Entity;
        }

        public async Task<GridLayout> PutAsync(GridLayout record)
        {       
            var entity = _context.Update(record);      
            entity.Property(x => x.Id).IsModified = false;     
            await _context.SaveChangesAsync();
            return entity.Entity;
        }

        public GridLayout Put(GridLayout record)
        {
            var entity = _context.Update(record);
            entity.Property(x => x.Id).IsModified = false;
            _context.SaveChanges();
            return entity.Entity;
        }

    }
}
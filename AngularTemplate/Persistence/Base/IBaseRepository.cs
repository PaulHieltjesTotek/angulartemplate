using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Base;

namespace Persistence.Base
{
    public interface IBaseRepository<T> where T : BaseModel
    {
        Task<T> GetAsync(Guid id);
        T Get(Guid id);
        Task<IEnumerable<T>> GetAllAsync();
        IEnumerable<T> GetAll();
        Task DeleteAsync(Guid id);
        void Delete(Guid id);
        Task<T> PostAsync(T record);
        T Post(T record);
        Task<T> PutAsync(T record);
        T Put(T record);
        T Upsert(T record);
        Task<T> UpsertAsync(T record);
        Task<IEnumerable<T>> PostBatchAsync(IEnumerable<T> records);
        IEnumerable<T> PostBatch(IEnumerable<T> records);
        Task<IEnumerable<T>> PutBatchAsync(IEnumerable<T> records);
        IEnumerable<T> PutBatch(IEnumerable<T> records);
        IEnumerable<T> UpsertBatch(IEnumerable<T> records);
        Task<IEnumerable<T>> UpsertBatchAsync(IEnumerable<T> records);
    }
}
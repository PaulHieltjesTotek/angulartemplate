using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Persistence.Base
{
	public abstract class BaseRepository<T, TA> : IBaseRepository<T> where T : BaseModel where TA : Microsoft.EntityFrameworkCore.DbContext
	{
		protected readonly IServiceScopeFactory _contextFactory;
		private readonly ILogger<BaseRepository<T, TA>> _logger;
		
		public BaseRepository(IServiceScopeFactory contextFactory, ILogger<BaseRepository<T, TA>> logger)
		{
			_logger = logger;
			_contextFactory = contextFactory;
		}

		protected TA GetContext()
		{
			return _contextFactory.CreateScope().ServiceProvider.GetRequiredService<TA>();
		}

		protected DbSet<T> GetDbSetModels(Microsoft.EntityFrameworkCore.DbContext context)
		{
			var dbSetProperty = context.GetType().GetProperties().FirstOrDefault(p => p.PropertyType == typeof(DbSet<T>));

			if (dbSetProperty == null)
			{
				_logger.LogCritical($"No dbSet {typeof(T)} found on dbContext {context.GetType()}");
				throw new Exception($"No dbSet {typeof(T)} found on dbContext {context.GetType()}");
			}
		
			return dbSetProperty.GetValue(context) as DbSet<T>;
		}
		
		public virtual async Task<T> GetAsync(Guid id)
		{
			using (var context = GetContext())
			{
				return await GetDbSetModels(context).FirstOrDefaultAsync(l => l.Id == id);
			}
		}

		public virtual T Get(Guid id)
		{
			using (var context = GetContext())
			{
				return GetDbSetModels(context).FirstOrDefault(l => l.Id == id);
			}
		}

		public virtual async Task<IEnumerable<T>> GetAllAsync()
		{
			using (var context = GetContext())
			{
				return await GetDbSetModels(context).ToListAsync();
			}
		}

		public virtual IEnumerable<T> GetAll()
		{
			using (var context = GetContext())
			{
				return GetDbSetModels(context).ToList();
			}
		}
		
		public virtual async Task DeleteAsync(Guid id)
		{
			using (var context = GetContext())
			{
				var models = GetDbSetModels(context);
				var entity = await models.FirstOrDefaultAsync(m => m.Id.Equals(id));

				if (entity != null)
				{
					models.Remove(entity);
					await context.SaveChangesAsync();
				}
				else
				{
					throw new NullReferenceException($"No record found with id: {id.ToString()}");
				}
			}
		}

		public virtual void Delete(Guid id)
		{
			using (var context = GetContext())
			{
				var models = GetDbSetModels(context);
				var entity = models.FirstOrDefault(m => m.Id.Equals(id));

				if (entity != null)
				{
					models.Remove(entity);
					context.SaveChanges();
				}
				else
				{
					throw new NullReferenceException($"No record found with id: {id.ToString()}");
				}
			}
		}
		
		public virtual async Task<T> PostAsync(T record)
		{
			using (var context = GetContext())
			{
				var entity = await GetDbSetModels(context).AddAsync(record);
				await context.SaveChangesAsync();
				return entity.Entity;
			}
		}

		public virtual T Post(T record)
		{
			using (var context = GetContext())
			{
				var entity = GetDbSetModels(context).Add(record);
				context.SaveChanges();
				return entity.Entity;
			}
		}

		public virtual T Upsert(T record)
		{
			return Get(record.Id) == null 
				? Post(record) 
				: Put(record);
		}
		
		public virtual Task<T> UpsertAsync(T record)
		{
			return Get(record.Id) == null 
				? PostAsync(record) 
				: PutAsync(record);
		}

		public virtual async Task<T> PutAsync(T record)
		{
			using (var context = GetContext())
			{
				var entity = context.Update(record);
				entity.Property(x => x.Id).IsModified = false;
				await context.SaveChangesAsync();
				return entity.Entity;
			}
		}

		public virtual T Put(T record)
		{
			using (var context = GetContext())
			{
				var entity = context.Update(record);
				entity.Property(x => x.Id).IsModified = false;
				context.SaveChanges();
				return entity.Entity;
			}
		}
		
		public virtual async Task<IEnumerable<T>> PostBatchAsync(IEnumerable<T> records)
		{
			using (var context = GetContext())
			{
				var result = records.Select(r => Add(r, context));
				await context.SaveChangesAsync();
				return result;
			}
		}

		public virtual IEnumerable<T> PostBatch(IEnumerable<T> records)
		{
			using (var context = GetContext())
			{
				var result = records.Select(r => Add(r, context));
				context.SaveChanges();
				return result;
			}
		}
		
		public virtual async Task<IEnumerable<T>> PutBatchAsync(IEnumerable<T> records)
		{
			using (var context = GetContext())
			{
				var result = records.Select(r => Update(r, context));
				await context.SaveChangesAsync();
				return result;
			}
		}

		public virtual IEnumerable<T> PutBatch(IEnumerable<T> records)
		{
			using (var context = GetContext())
			{
				var result = records.Select(r => Update(r, context));
				context.SaveChanges();
				return result;
			}
		}
		
		public virtual IEnumerable<T> UpsertBatch(IEnumerable<T> records)
		{
			using (var context = GetContext())
			{
				var result = records.Select(record => Get(record.Id) == null
					? Add(record, context)
					: Update(record, context));
				context.SaveChanges();
				return result;
			}
		}
		
		public virtual async Task<IEnumerable<T>> UpsertBatchAsync(IEnumerable<T> records)
		{
			using (var context = GetContext())
			{
				var result = records.Select(record => Get(record.Id) == null
					? Add(record, context)
					: Update(record, context));
				await context.SaveChangesAsync();
				return result;
			}
		}

		private T Update(T record, TA context)
		{
			var entity = context.Update(record);
			entity.Property(x => x.Id).IsModified = false;
			return entity.Entity;
		}
		
		private T Add(T record, TA context)
		{
			var entity = GetDbSetModels(context).Add(record);
			return entity.Entity;
		}
	}
}
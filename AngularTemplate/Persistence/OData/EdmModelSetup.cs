using Domain.Models;
using Domain.Models.Application;
using Microsoft.AspNet.OData.Builder;
using Microsoft.OData.Edm;
using Shared.Models.Authentication;
using tomware.Microwf.AspNetCoreEngine.Domain;

namespace Persistence.OData
{
    public static class EdmModelSetup
    {
        public static IEdmModel GetEdmModel()
        {
            var builder = new ODataConventionModelBuilder();
            builder.EnableLowerCamelCase(NameResolverOptions.ProcessReflectedPropertyNames | NameResolverOptions.ProcessExplicitPropertyNames);
            
            builder.EntitySet<Workflow>("ODataWorkflow");
            builder.EntitySet<WorkflowHistory>("ODataWorkflowHistory");
            builder.EntitySet<AppUser>("ODataUser");
            builder.EntitySet<Document>("ODataDocument");
            return builder.GetEdmModel();
        } 
    }
}
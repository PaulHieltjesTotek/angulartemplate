using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Extensions;
using DbContext;
using Domain;
using Domain.Models.Application;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Shared.Models.Authentication;

namespace Persistence.DataSeeding
{
    public class AngularDatabaseSeeding
    {
        public static async Task Initialize(IServiceProvider serviceProvider)
        {
            await SeedIdentity(serviceProvider);
            SeedApplication(serviceProvider);
        }
        
        private static async Task SeedIdentity(IServiceProvider serviceProvider)
        {
            using (var serviceScope = serviceProvider.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<IdentityContext>();

                var roleStore = new RoleStore<IdentityRole>(context);

                foreach (var role in typeof(UserRoles).GetAllPublicConstantValues<string>())
                {
                    if (!context.Roles.Any(r => r.Name == role))
                    {
                        await roleStore.CreateAsync(new IdentityRole {Name = role, NormalizedName = role});
                    }
                }

                if (context.Users.Count(u => u.UserName == "admin") < 1)
                {
                    var user = new AppUser()
                    {
                        UserName = "admin",
                        NormalizedUserName = "admin",
                        Email = "admin@email.com",
                        NormalizedEmail = "admin@email.com",
                        EmailConfirmed = true,
                        LockoutEnabled = false,
                        SecurityStamp = Guid.NewGuid().ToString(),  
                        Active = true,
                        LandingPage = "Dashboard"
                    };

                    var password = new PasswordHasher<AppUser>();
                    var hashed = password.HashPassword(user, "Admin@Tjipa");
                    user.PasswordHash = hashed;
                    var userStore = new UserStore<AppUser>(context);
                    await userStore.CreateAsync(user);
                    await userStore.AddToRoleAsync(user, UserRoles.AdminRole);
                }


                context.SaveChanges();
            }
        }
        
        private static void SeedApplication(IServiceProvider serviceProvider)
        {
            using (var serviceScope = serviceProvider.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<ApplicationContext>();


                context.SaveChanges();
            }
        }

    }
}
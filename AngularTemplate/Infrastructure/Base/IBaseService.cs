using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;
using Domain.Base;

namespace Infrastructure.Base
{
    public interface IBaseService<T> where T : BaseModel
    {
        Task<ReturnModel<T>> GetAsync(Guid id);
        Task<ReturnModel<IEnumerable<T>>> GetAllAsync();
        Task<ReturnModel<T>> PostAsync(T model);
        Task<ReturnModel<T>> PutAsync(T model);
        Task<ReturnModel<string>> DeleteAsync(Guid id);
        ReturnModel<T> Get(Guid id);
        ReturnModel<IEnumerable<T>> GetAll();
        ReturnModel<T> Post(T model);
        ReturnModel<T> Put(T model);
        ReturnModel<string> Delete(Guid id);
        Task<ReturnModel<T>> UpsertAsync(T model);
        ReturnModel<T> Upsert(T model);
    }
}
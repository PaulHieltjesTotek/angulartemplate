using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Core.Models;
using Domain.Base;
using FluentValidation;
using Persistence.Base;

namespace Infrastructure.Base
{
    public abstract class BaseService<T> : IBaseService<T> where T : BaseModel
    {
        private readonly IBaseRepository<T> _repository;
        
        protected BaseService(IBaseRepository<T> repository)
        {
            _repository = repository;
        }
        
        
        /// <summary>
        /// gets a single object of type T
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<ReturnModel<T>> GetAsync(Guid id)
        {
            try
            {
                return ReturnModel<T>.ReturnSuccess(await _repository.GetAsync(id));
            }
            catch (Exception e)
            {
                return ReturnModel<T>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// gets all objects of type T
        /// </summary>
        /// <returns></returns>
        public virtual async Task<ReturnModel<IEnumerable<T>>> GetAllAsync()
        {
            try
            {
                return ReturnModel<IEnumerable<T>>.ReturnSuccess(await _repository.GetAllAsync());
            }
            catch (Exception e)
            {
                return ReturnModel<IEnumerable<T>>.ReturnError(e);
            }
        }

        /// <summary>
        /// posts a new object of type T
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<ReturnModel<T>> PostAsync(T model)
        {
            try
            {
                var (isSuccessful, errors) = await ValidateModelAsync(model);

                return isSuccessful
                    ? ReturnModel<T>.ReturnSuccess(await _repository.PostAsync(model))
                    : ReturnModel<T>.ReturnError(errors);
            }
            catch (Exception e)
            {
                return ReturnModel<T>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// updates an exisiting object of type T
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<ReturnModel<T>> PutAsync(T model)
        {
            try
            {
                var (isSuccessful, errors) = await ValidateModelAsync(model);

                return isSuccessful
                    ? ReturnModel<T>.ReturnSuccess(await _repository.PutAsync(model))
                    : ReturnModel<T>.ReturnError(errors);
            }
            catch (Exception e)
            {
                return ReturnModel<T>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// posts a new object of type T if it doesn't exist. If it does it will update the existing object.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual async Task<ReturnModel<T>> UpsertAsync(T model)
        {
            try
            {
                var (isSuccessful, errors) = await ValidateModelAsync(model);

                return isSuccessful
                    ? ReturnModel<T>.ReturnSuccess(await _repository.UpsertAsync(model))
                    : ReturnModel<T>.ReturnError(errors);
            }
            catch (Exception e)
            {
                return ReturnModel<T>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// deletes a single object of type T
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<ReturnModel<string>> DeleteAsync(Guid id)
        {
            try
            {
                await _repository.DeleteAsync(id);
                return ReturnModel<string>.ReturnSuccess("Deleted");
            }
            catch (Exception e)
            {
                return ReturnModel<string>.ReturnError(e);
            }
        }
        
        
        /// <summary>
        /// gets a single object of type T
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual ReturnModel<T> Get(Guid id)
        {
            try
            {
                return ReturnModel<T>.ReturnSuccess(_repository.Get(id));
            }
            catch (Exception e)
            {
                return ReturnModel<T>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// gets all objects of type T
        /// </summary>
        /// <returns></returns>
        public virtual ReturnModel<IEnumerable<T>> GetAll()
        {
            try
            {
                return ReturnModel<IEnumerable<T>>.ReturnSuccess(_repository.GetAll());
            }
            catch (Exception e)
            {
                return ReturnModel<IEnumerable<T>>.ReturnError(e);
            }
        }

        /// <summary>
        /// Adds a new object of type T
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual ReturnModel<T> Post(T model)
        {
            try
            {
                var (isSuccessful, errors) = ValidateModel(model);

                return isSuccessful
                    ? ReturnModel<T>.ReturnSuccess(_repository.Post(model))
                    : ReturnModel<T>.ReturnError(errors);
            }
            catch (Exception e)
            {
                return ReturnModel<T>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// Updates a single object of type T
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual ReturnModel<T> Put(T model)
        {
            try
            {
                var (isSuccessful, errors) = ValidateModel(model);

                return isSuccessful
                    ? ReturnModel<T>.ReturnSuccess(_repository.Put(model))
                    : ReturnModel<T>.ReturnError(errors);
            }
            catch (Exception e)
            {
                return ReturnModel<T>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// adds or updates a single object of type T
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public virtual ReturnModel<T> Upsert(T model)
        {
            try
            {
                var (isSuccessful, errors) = ValidateModel(model);

                return isSuccessful
                    ? ReturnModel<T>.ReturnSuccess(_repository.Upsert(model))
                    : ReturnModel<T>.ReturnError(errors);
            }
            catch (Exception e)
            {
                return ReturnModel<T>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// deletes a single object of type T
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual ReturnModel<string> Delete(Guid id)
        {
            try
            {
                _repository.Delete(id);
                return ReturnModel<string>.ReturnSuccess("Deleted");
            }
            catch (Exception e)
            {
                return ReturnModel<string>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// validates the model for valid entries
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private(bool isSuccessful, string errors) ValidateModel(T model)
        {
            try
            {
                if (!typeof(BaseConfigurator).IsAssignableFrom(typeof(T)))
                {
                    return (true, string.Empty);
                }

                var config = model as BaseConfigurator;
                
                config.Configurate();

                Assembly.Load("Domain");
                var assembly = AppDomain.CurrentDomain.GetAssemblies()
                    .SingleOrDefault(a => a.GetName().Name == "Domain");
                if (assembly != null)
                {
                    var listOfValidators =
                        assembly.GetTypes().Where(a => typeof(AbstractValidator<T>).IsAssignableFrom(a));


                    foreach (var validator in listOfValidators)
                    {
                        var v = (AbstractValidator<T>)Activator.CreateInstance(validator);
                        var result = v.Validate(model);
                
                        if (!result.IsValid) return (false, string.Join("\n", result.Errors));
                    }

                }
                
                //config.ApplyBusinessLogic();
                
                //again if all good: add it
                return (true, string.Empty);

            }
            catch (Exception e)
            {
                return (false, e.Message);
            }
        }

        /// <summary>
        /// validates the model for valid entries
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private async Task<(bool isSuccessful, string errors)> ValidateModelAsync(T model)
        {
            try
            {
                if (!typeof(BaseConfigurator).IsAssignableFrom(typeof(T)))
                {
                    return (true, string.Empty);
                }

                var config = model as BaseConfigurator;
                
                config.Configurate();
                
                Assembly.Load("Domain");
                var assembly = AppDomain.CurrentDomain.GetAssemblies()
                    .SingleOrDefault(a => a.GetName().Name == "Domain");
                if (assembly != null)
                {
                    var listOfValidators =
                        assembly.GetTypes().Where(a => typeof(AbstractValidator<T>).IsAssignableFrom(a));


                    foreach (var validator in listOfValidators)
                    {
                        var v = (AbstractValidator<T>)Activator.CreateInstance(validator);
                        var result = await v.ValidateAsync(model);
                
                        if (!result.IsValid) return (false, string.Join("\n", result.Errors));
                    }

                }

                //config.ApplyBusinessLogic();
                
                //again if all good: add it
                return (true, string.Empty);

            }
            catch (Exception e)
            {
                return (false, e.Message);
            }
        }
        
    }
}
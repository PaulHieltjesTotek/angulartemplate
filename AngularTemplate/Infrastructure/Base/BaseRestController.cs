using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;
using Domain.Base;
using Microsoft.AspNetCore.Mvc;

namespace Infrastructure.Base
{
    public abstract class BaseRestController<T> : Controller where T : BaseModel 
    {
        private readonly IBaseService<T> _service;


        protected BaseRestController(IBaseService<T> service)
        {
            _service = service;
        }
        
        [HttpGet("{id}")]
        public virtual async Task<ReturnModel<T>> Get(Guid id)
        {
            return await _service.GetAsync(id);
        }

        [HttpGet]
        public virtual async Task<ReturnModel<IEnumerable<T>>> Get()
        {
            return await _service.GetAllAsync();
        }
        
        [HttpPost]
        public virtual async Task<ReturnModel<T>> Post([FromBody] T model)
        {
            return await _service.PostAsync(model);
        }
        
        [HttpPost("[action]")]
        public virtual async Task<ReturnModel<T>> Upsert([FromBody] T model)
        {
            return await _service.UpsertAsync(model);
        }
        
        [HttpPut]
        public virtual async Task<ReturnModel<T>> Put([FromBody] T model)
        {
            return await _service.PutAsync(model);
        }
        
        [HttpDelete("{id}")]
        public virtual async Task<ReturnModel<string>> Delete(Guid id)
        {
            return await _service.DeleteAsync(id);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Attributes;
using Core.Models;
using Domain.Models.Application;
using Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Persistence.Repositories.Interfaces;
using Shared.Models.Authentication;

namespace Infrastructure.Services
{
    [AutoDi(AddType.Transiant, typeof(IGridLayoutService))]
    public class GridLayoutService : IGridLayoutService
    {
        private readonly IGridLayoutRepository _repository;
        private readonly IHttpContextAccessor _accessor;
        private readonly UserManager<AppUser> _userManager;
        
        public GridLayoutService(IGridLayoutRepository repository, IHttpContextAccessor accessor, UserManager<AppUser> userManager)
        {
            _repository = repository;
            _accessor = accessor;
            _userManager = userManager;
        }

        /// <summary>
        /// returns the current userId
        /// </summary>
        /// <returns></returns>
        private Guid GetUserId()
        {
            if(_accessor.HttpContext == null) return Guid.Empty;
        
            var user = _userManager?.Users.FirstOrDefault(u => u.UserName.Equals(_userManager.GetUserId(_accessor.HttpContext.User), StringComparison.InvariantCultureIgnoreCase));
            
            if(user == null) return Guid.Empty;

            return Guid.TryParse(user.Id, out var id) 
                ? id 
                : Guid.Empty;
        }
        
        /// <summary>
        /// Return the gridlayouts for the provided grid
        /// </summary>
        /// <param name="gridName"></param>
        /// <returns></returns>
        public async Task<ReturnModel<IEnumerable<GridLayout>>> GetAllForGridAsync(string gridName)
        {
            try
            {
                return ReturnModel<IEnumerable<GridLayout>>.ReturnSuccess(await _repository.GetAllForGridAsync(gridName, GetUserId()));
            }
            catch (Exception e)
            {
                return ReturnModel<IEnumerable<GridLayout>>.ReturnError(e);
            }
        }

        /// <summary>
        /// Return the gridlayouts for the provided grid
        /// </summary>
        /// <param name="gridName"></param>
        /// <returns></returns>
        public ReturnModel<IEnumerable<GridLayout>> GetAllForGrid(string gridName)
        {
            try
            {
                return ReturnModel<IEnumerable<GridLayout>>.ReturnSuccess(_repository.GetAllForGrid(gridName, GetUserId()));
            }
            catch (Exception e)
            {
                return ReturnModel<IEnumerable<GridLayout>>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// posts a new grid layout
        /// </summary>
        /// <param name="layout"></param>
        /// <returns></returns>
        public async Task<ReturnModel<GridLayout>> Post(GridLayout layout)
        {
            layout.UserId = GetUserId();

            try
            {
                return ReturnModel<GridLayout>.ReturnSuccess(await _repository.PostAsync(layout));
            }
            catch (Exception e)
            {
                return ReturnModel<GridLayout>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// updates a grid layout
        /// </summary>
        /// <param name="layout"></param>
        /// <returns></returns>
        public async Task<ReturnModel<GridLayout>> Put(GridLayout layout)
        {
            if (layout.UserId != GetUserId())
            {
                return ReturnModel<GridLayout>.ReturnError("You are not allowed to save this layout"); 
            }
            
            try
            {
                return ReturnModel<GridLayout>.ReturnSuccess(await _repository.PutAsync(layout));
            }
            catch (Exception e)
            {
                return ReturnModel<GridLayout>.ReturnError(e);
            }
        }
        
        /// <summary>
        /// deletes a grid layout
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ReturnModel<string> Delete(Guid id)
        {
            try
            {
                var layout = _repository.Get(id);         
                if (layout.UserId != GetUserId())
                {
                    return ReturnModel<string>.ReturnError("You are not allowed to delete this layout"); 
                }

                _repository.Delete(layout.Id);
                return ReturnModel<string>.ReturnSuccess("Deleted");
            }
            catch (Exception e)
            {
                return ReturnModel<string>.ReturnError(e);
            }
        }
    }
}
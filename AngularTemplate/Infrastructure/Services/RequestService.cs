using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Attributes;
using Core.Models;
using DbContext;
using Domain;
using Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Shared.Models.Authentication;

namespace Infrastructure.Services
{
    [AutoDi(AddType.Transiant, typeof(IRequestService))]
    public class RequestService : IRequestService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly ILogger<UserService> _logger;
        private readonly IHttpContextAccessor _accessor;
        private readonly IServiceScopeFactory _contextFactory;
        
        public RequestService(UserManager<AppUser> userManager, ILogger<UserService> logger, IHttpContextAccessor accessor, IServiceScopeFactory contextFactory)
        {
            _userManager = userManager;
            _logger = logger;
            _accessor = accessor;
            _contextFactory = contextFactory;
        }

        /// <summary>
        /// returns the current users language
        /// </summary>
        /// <returns></returns>
        public string GetCurrentLanguage()
        {
            if (_accessor.HttpContext.Request.Headers.ContainsKey("Language"))
            {
                return _accessor.HttpContext.Request.Headers["Language"].ToString().ToLower();
            }

            return "en";
        }

        /// <summary>
        /// returns the current users usersname
        /// </summary>
        /// <returns></returns>
        public string GetCurrentUserName()
        {
            return _userManager.GetUserId(_accessor.HttpContext.User).ToLower();
        }
        
        /// <summary>
        /// returns current users id
        /// </summary>
        /// <returns></returns>
        public Guid GetCurrentUserId()
        {
            return Guid.TryParse(GetCurrentIdentityUser().Id, out var id) 
                ? id 
                : Guid.Empty;
        }

        /// <summary>
        /// returns current users identity
        /// </summary>
        /// <returns></returns>
        public AppUser GetCurrentIdentityUser()
        {
            
            return _userManager.Users.FirstOrDefault(u => u.UserName.Equals(GetCurrentUserName(), StringComparison.InvariantCultureIgnoreCase));
        }
        

        /// <summary>
        /// returns the currents users roles
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<string>> GetCurrentRoles()
        {
            return await _userManager.GetRolesAsync(GetCurrentIdentityUser());
        }

        /// <summary>
        /// returns the current applicaiton base URL
        /// </summary>
        /// <returns></returns>
        public string GetCurrentUrl()
        {
            return _accessor.HttpContext.Request.Host.ToString();
        }
    }
}
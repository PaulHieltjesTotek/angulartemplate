using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core.Attributes;
using Core.Models;
using Domain.Models;
using Domain.Models.Application;
using Infrastructure.Base;
using Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Scaffolding.Internal;
using Newtonsoft.Json;
using Persistence.Base;
using Persistence.Repositories.Interfaces;
using Shared.ViewModels;

namespace Infrastructure.Services
{
    [AutoDi(AddType.Transiant, typeof(IDocumentService))]
    public class DocumentService : BaseService<Document>, IDocumentService
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IDocumentRepository _repository;
        private readonly IPickListRepository _pickListRepository;
        private readonly IRequestService _requestService;
        public DocumentService(IDocumentRepository repository, IHostingEnvironment hostingEnvironment, IPickListRepository pickListRepository,
            IRequestService requestService) : base(repository)
        {
            _hostingEnvironment = hostingEnvironment;
            _repository = repository;
            _pickListRepository = pickListRepository;
            _requestService = requestService;
        }

        /// <summary>
        /// deletes a document/upload
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public override async Task<ReturnModel<string>> DeleteAsync(Guid id)
        {
            var doc = _repository.Get(id);

            if (doc == null)
            {
                throw new Exception("Document does not exist.");
            }
            
            //check if the file name is a valid GUID (this is to prevent users from downloading a different file from a different folder)
            if (!Guid.TryParse(doc.FileId, out var i))
            {
                throw new Exception("Documents file id is not valid.");
            }
            var filePath = Path.Combine(_hostingEnvironment.WebRootPath, "uploads", doc.FileId);
            File.Delete(filePath);
            
            return await base.DeleteAsync(id);
        }

        /// <summary>
        /// uploads a new file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public ReturnModel<string> UploadChunk(IFormFile file) {
            var tempPath = Path.Combine(_hostingEnvironment.WebRootPath, "uploads");
            Directory.CreateDirectory(tempPath);
            var fileName = Guid.NewGuid().ToString();
            //var fileName = fileId + "." + file.FileName.Split(".").LastOrDefault();
            try
            {
                using (var stream = file.OpenReadStream())
                {
                    using (var fileStream = File.Create(Path.Combine(tempPath, fileName)))
                    {
                        stream.Seek(0, SeekOrigin.Begin);
                        stream.CopyTo(fileStream);
                    }
                }
                
            } catch (Exception e) {
                return ReturnModel<string>.ReturnError(e);
            }
            return ReturnModel<string>.ReturnSuccess(fileName);
        }
        
        /// <summary>
        /// save the file stream
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public ReturnModel<string> SaveStream(Stream stream) {
            var tempPath = Path.Combine(_hostingEnvironment.WebRootPath, "uploads");
            Directory.CreateDirectory(tempPath);
            var fileName = Guid.NewGuid().ToString();
            try
            {
                using (var fileStream = File.Create(Path.Combine(tempPath, fileName)))
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    stream.CopyTo(fileStream);
                }

            } catch (Exception e) {
                return ReturnModel<string>.ReturnError(e);
            }
            return ReturnModel<string>.ReturnSuccess(fileName);
        }

        /// <summary>
        /// download file. A check here is done, only users that uploaded the file are allowed to download it
        /// Unless you have a AirPlus BO role, if so then you can download all files
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public (FileStream, string) DownloadDocument(Guid id)
        {
            var doc = _repository.Get(id);

            if (doc == null)
            {
                throw new Exception("Document does not exist.");
            }
            
            //check if you are allowed to retrieve this

            if (doc.CreatedBy != _requestService.GetCurrentUserId())
            {
                throw new Exception("You are not allowed to download this document.");
            }

            //check if the file name is a valid GUID (this is to prevent users from downloading a different file from a different folder)
            if (!Guid.TryParse(doc.FileId, out var i))
            {
                throw new Exception("Documents file id is not valid.");
            }

            var filePath = Path.Combine(_hostingEnvironment.WebRootPath, "uploads", doc.FileId);//$"{doc.FileId}.{doc.FileExtension}");
            
            
            //HttpContext.Current.Response.ContentType = "text/xml";
           return (File.OpenRead(filePath), doc.FileExtension);
            //return File(stream, "application/pdf", "FileDownloadName.ext");
        }
        
        /// <summary>
        /// check if the file extention is valid
        /// </summary>
        /// <param name="fileName"></param>
        /// <exception cref="Exception"></exception>
        public void CheckFileExtensionValid(string fileName) {
            fileName = fileName.ToLower();
            string[] imageExtensions = { ".jpg", ".jpeg", ".gif", ".png" };
            
            var isValidExtenstion = imageExtensions.Any(ext => fileName.LastIndexOf(ext) > -1);
            if (!isValidExtenstion)
            {
                throw new Exception("Not allowed file extension");
            }
        }
        
        /// <summary>
        /// check file size
        /// </summary>
        /// <param name="stream"></param>
        /// <exception cref="Exception"></exception>
        private void CheckMaxFileSize(FileStream stream) {
            if (stream.Length > 4000000)
            {
                throw new Exception("File is too large");
            }
        }
        
    }
}
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Core;
using Core.Attributes;
using Core.Helpers;
using Core.Models;
using Domain;
using Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Shared.Models.Authentication;
using Shared.ViewModels;

namespace Infrastructure.Services
{
    [AutoDi(AddType.Transiant, typeof(IUserService))]
    public class UserService: IUserService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly IRequestService _requestService;
        private readonly IEmailService _emailService;
        public UserService(UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager, IOptions<JwtIssuerOptions> jwtOptions, 
            IRequestService request, IEmailService emailService)
        {
            _userManager = userManager;
            _jwtOptions = jwtOptions.Value;
            _requestService = request;
            _roleManager = roleManager;
            _emailService = emailService;
        }
        
        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<ReturnModel<bool>> CreateUser(UserCreationViewModel user)
        {
            try
            {
                var existingUser = _userManager.Users.FirstOrDefault(u =>
                    u.UserName.Equals(user.UserName, StringComparison.InvariantCultureIgnoreCase));//_userManager.FindByNameAsync(user.UserName);

                if (existingUser != null)
                {
                    return ReturnModel<bool>.ReturnError("User already exists.");
                }

                var appUser = new AppUser()
                {
                    UserName = user.UserName,
                    Email = user.Email,
                    LandingPage = "MyCardRequest",
                    Active = user.Activate
                };
                //temp password, will be regenerated before activating
                var password = PasswordHelper.CreatePassword(10);
                var result = await _userManager.CreateAsync(appUser, password);

                appUser.LockoutEnabled = false;
                var a = await _userManager.UpdateAsync(appUser);

                if (!result.Succeeded)
                {
                    ReturnModel<bool>.ReturnError(result.Errors.Select(e => e.Description));
                }

                if (!string.IsNullOrWhiteSpace(user.InitialRole))
                {
                    var r = await AddRoleToUser(new Guid(appUser.Id), user.InitialRole);
                }

                if (user.Activate)
                {
                    _emailService.SendNewUser(appUser.Email, appUser.UserName, password);
                }
                
                return result.Succeeded 
                    ? ReturnModel<bool>.ReturnSuccess(true) 
                    : ReturnModel<bool>.ReturnError(result.Errors.Select(e => e.Description));
            }
            catch (Exception e)
            {
                return ReturnModel<bool>.ReturnError("Something went wrong: " + e.Message);
            }
        }
        
        /// <summary>
        /// deletes a user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ReturnModel<bool>> DeleteUser(Guid id)
        {
            try
            {
                var existingUser = await _userManager.FindByIdAsync(id.ToString());

                if (existingUser == null)
                {
                    return ReturnModel<bool>.ReturnError("User does not exist.");
                }

                var result = await _userManager.DeleteAsync(existingUser);

                if (!result.Succeeded) return ReturnModel<bool>.ReturnError(result.Errors.Select(e => e.Description));

                return ReturnModel<bool>.ReturnSuccess(true);
            }
            catch (Exception e)
            {
                return ReturnModel<bool>.ReturnError("Something went wrong: " + e.Message);
            }
        }

        /// <summary>
        /// Set the provided user active or inactive
        /// </summary>
        /// <param name="id">The user id</param>
        /// <param name="active"></param>
        /// <returns></returns>
        public async Task<ReturnModel<bool>> SetActivenessUser(Guid id, bool active)
        {
            try
            {
                var existingUser = await _userManager.FindByIdAsync(id.ToString());

                if (existingUser == null)
                {
                    return ReturnModel<bool>.ReturnError("User does not exist.");
                }
                
                existingUser.Active = active;
                existingUser.LockoutEnabled = false;
                var result = await _userManager.UpdateAsync(existingUser);

                if (!result.Succeeded) return ReturnModel<bool>.ReturnError(result.Errors.Select(e => e.Description));

                return ReturnModel<bool>.ReturnSuccess(true);
            }
            catch (Exception e)
            {
                return ReturnModel<bool>.ReturnError("Something went wrong: " + e.Message);
            }
        }

        /// <summary>
        /// Activate the user. This will also sent a welcome email and langingpage
        /// </summary>
        /// <param name="id"></param>
        /// <param name="landingPage"></param>
        /// <returns></returns>
        public async Task<ReturnModel<(bool existing, AppUser user, string password)>> ActivateUser(Guid id, string landingPage = null)
        {
            try
            {
                var existingUser = await _userManager.FindByIdAsync(id.ToString());

                if (existingUser == null)
                {
                    return ReturnModel<(bool existing, AppUser user, string password)>.ReturnError("User does not exist.");
                }

                if (existingUser.Active)
                {
                    //already active
                    return ReturnModel<(bool existing, AppUser user, string password)>.ReturnSuccess((true, existingUser, string.Empty));
                }
                
                var password = PasswordHelper.CreatePassword(10);
                
                
                var hasher = new PasswordHasher<AppUser>();
                var hashed = hasher.HashPassword(existingUser, password);
                existingUser.Active = true;
                existingUser.LandingPage = landingPage;
                existingUser.PasswordHash = hashed;
                existingUser.LockoutEnabled = false;


                var result = await _userManager.UpdateAsync(existingUser);

                if (!result.Succeeded) return ReturnModel<(bool existing, AppUser user, string password)>.ReturnError(result.Errors.Select(e => e.Description));

                if (landingPage == null)
                {
                    var r = await SetLandingPage(existingUser);
                    
                    if (!r.IsSuccessful) return ReturnModel<(bool existing, AppUser user, string password)>.ReturnError(r.Errors);
                }
                
                return ReturnModel<(bool existing, AppUser user, string password)>.ReturnSuccess((false, existingUser, password));
            }
            catch (Exception e)
            {
                return ReturnModel<(bool existing, AppUser user, string password)>.ReturnError("Something went wrong: " + e.Message);
            }
        }

        /// <summary>
        /// register a new user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ReturnModel<Guid>> RegisterUser(RegistrationViewModel model)
        {
            //var userIdentity = _mapper.Map<AppUser>(model);
            
            var userIdentity = Mapper.Map<AppUser>(model);
            userIdentity.UserName = model.Email;
            
            var result = await _userManager.CreateAsync(userIdentity, model.Password);
            
            userIdentity.LockoutEnabled = false;
            var a = await _userManager.UpdateAsync(userIdentity);

            if (!result.Succeeded) return ReturnModel<Guid>.ReturnError(
                string.Join("\n", result.Errors.Select(e => $"{e.Code} - {e.Description}")));

            return ReturnModel<Guid>.ReturnSuccess(new Guid(userIdentity.Id));
        }

        /// <summary>
        /// reset password for the provided username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<ReturnModel<bool>> ResetPassword(string username)
        {

            var user = _userManager.Users.FirstOrDefault(u => u.UserName.Equals(username, StringComparison.InvariantCultureIgnoreCase));

            if (user == null)
            {
                switch (_requestService.GetCurrentLanguage())
                {
                    case "nl":
                        return ReturnModel<bool>.ReturnError("De gebruiker bestaat niet.");
                    case "fr":
                        return ReturnModel<bool>.ReturnError("L'utilisateur n'existe pas");
                    default:
                        return ReturnModel<bool>.ReturnError("The user does not exist.");
                }
            }
            
            var password = PasswordHelper.CreatePassword(10);
            var hasher = new PasswordHasher<AppUser>();
            var hashed = hasher.HashPassword(user, password);
            
            user.EmailConfirmed = false;
            user.AccessFailedCount = 0;
            user.LockoutEnabled = false;
            user.PasswordHash = hashed;
            
            var result = await _userManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                return ReturnModel<bool>.ReturnError(
                    string.Join("\n", result.Errors.Select(e => $"{e.Code} - {e.Description}")));
            }
            
            
            
            //send email
            _emailService.SendResetPassword(user.Email, password);


            return ReturnModel<bool>.ReturnSuccess(true);
        }

        /// <summary>
        /// change password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ReturnModel<bool>> ChangePassword(ChangePasswordViewModel model)
        {

            if (model.NewPassword != model.ConfirmPassword)
            {
                switch (_requestService.GetCurrentLanguage())
                {
                    case "nl":
                        return ReturnModel<bool>.ReturnError("Het nieuwe wachtwoord en het bevestigings wachtwoord zijn niet gelijk.");
                    case "fr":
                        return ReturnModel<bool>.ReturnError("Le nouveau mot de passe et le mot de passe de confirmation ne correspondent pas.");
                    default:
                        return ReturnModel<bool>.ReturnError("The new password and the confirm password do not match.");
                }
            }

            var username = _requestService.GetCurrentUserName();
            
            var login = await Login(new CredentialsViewModel {UserName = username, Password = model.CurrentPassword});
            if (!login.IsSuccessful)
            {
                return ReturnModel<bool>.ReturnError(login.Errors);
            }
            
            var user = _userManager.Users.First(u => u.UserName.Equals(username, StringComparison.InvariantCultureIgnoreCase));
            var hasher = new PasswordHasher<AppUser>();
            var hashed = hasher.HashPassword(user, model.NewPassword);
            
            user.EmailConfirmed = true;
            user.PasswordHash = hashed;
            
            var result = await _userManager.UpdateAsync(user);

            if (!result.Succeeded)
            {
                return ReturnModel<bool>.ReturnError(
                    string.Join("\n", result.Errors.Select(e => $"{e.Code} - {e.Description}")));
            }


            return ReturnModel<bool>.ReturnSuccess(true);
        }

        /// <summary>
        /// Tries to log in. On 5 fails attempts the user will be locked and a password reset is needed
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ReturnModel<string>> Login(CredentialsViewModel model)
        {
            var user = _userManager.Users.FirstOrDefault(u => u.UserName.Equals(model.UserName, StringComparison.InvariantCultureIgnoreCase));

            if (user?.LockoutEnabled == true)
            {
                switch (_requestService.GetCurrentLanguage())
                {
                    case "nl":
                        return ReturnModel<string>.ReturnError("Uw account is geblokkeerd omdat u te vaak een foutieve wachtwoord heeft ingevuld. Reset uw wachtwoord om uw account te deblokkeren.");
                    case "fr":
                        return ReturnModel<string>.ReturnError("Votre compte est verrouillé car vous avez entré un mot de passe incorrect plusieurs fois. Réinitialisez votre mot de passe pour déverrouiller votre compte.");
                    default:
                        return ReturnModel<string>.ReturnError("Your account is locked because you entered an incorrect password to many times. Reset your password to unlock your account.");
                }
            }
            
            var identity = await GetClaimsIdentity(model.UserName, model.Password);
            if (identity == null)
            {
                if (user != null)
                {
                    user.AccessFailedCount++;

                    if (user.AccessFailedCount > 5)
                    {
                        user.LockoutEnabled = true;
                        await _userManager.UpdateAsync(user);
                        
                        switch (_requestService.GetCurrentLanguage())
                        {
                            case "nl":
                                return ReturnModel<string>.ReturnError("Uw account is geblokkeerd omdat u te vaak een foutieve wachtwoord heeft ingevuld. Reset uw wachtwoord om uw account te deblokkeren.");
                            case "fr":
                                return ReturnModel<string>.ReturnError("Votre compte est verrouillé car vous avez entré un mot de passe incorrect plusieurs fois. Réinitialisez votre mot de passe pour déverrouiller votre compte.");
                            default:
                                return ReturnModel<string>.ReturnError("Your account is locked because you entered an incorrect password to many times. Reset your password to unlock your account.");
                        }
                    }
                    
                    await _userManager.UpdateAsync(user);
                }
                
                
                switch (_requestService.GetCurrentLanguage())
                {
                    case "nl":
                        return ReturnModel<string>.ReturnError("Foutieve gebruikersnaam of wachtwoord.");
                    case "fr":
                        return ReturnModel<string>.ReturnError("Nom d'utilisateur ou mot de passe invalide.");
                    default:
                        return ReturnModel<string>.ReturnError("Invalid username or password.");
                }
            }

            //var user = _userManager.FindByNameAsync(model.UserName).Result;
            
            user.AccessFailedCount = 0;
            var r = await _userManager.UpdateAsync(user);
            
            
            if (!user.Active)
            {
                switch (_requestService.GetCurrentLanguage())
                {
                    case "nl":
                        return ReturnModel<string>.ReturnError("Deze gebruiker is nog niet actief.");
                    case "fr":
                        return ReturnModel<string>.ReturnError("L'utilisateur n'est pas encore activé.");
                    default:
                        return ReturnModel<string>.ReturnError("The user is not activated yet.");
                }
            }
            
            // Serialize and return the response
            var response = new
            {
                id = identity.Claims.Single(c => c.Type == "id").Value,
                auth_token = await GenerateEncodedToken(model.UserName, identity),
                expires_in = (int)_jwtOptions.ValidFor.TotalSeconds,
                name = identity.Name,
                landing_page = user.LandingPage,
                first_login = !user.EmailConfirmed
            };

            var json = JsonConvert.SerializeObject(response);
            return ReturnModel<string>.ReturnSuccess(json);
        }

        /// <summary>
        /// Gets the roles for the provided user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ReturnModel<IEnumerable<string>>> GetRolesForUser(Guid userId)
        {
            
            try
            {
                var user = await _userManager.FindByIdAsync(userId.ToString());
                return ReturnModel<IEnumerable<string>>.ReturnSuccess(await _userManager.GetRolesAsync(user));
            }
            catch (Exception e)
            {
                return ReturnModel<IEnumerable<string>>.ReturnError(e);   
            }
        }

        /// <summary>
        /// gets all the missing roles of the provided user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ReturnModel<IEnumerable<string>>> GetMissingRolesForUser(Guid userId)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId.ToString());
                var activeRoles = await _userManager.GetRolesAsync(user);
                var missing = _roleManager.Roles.Select(f => f.NormalizedName).Where(r => !activeRoles.Contains(r));
                
                return ReturnModel<IEnumerable<string>>.ReturnSuccess(missing);
            }
            catch (Exception e)
            {
                return ReturnModel<IEnumerable<string>>.ReturnError(e);   
            }
        }

        /// <summary>
        /// Sets landing page of the user. This is based on the roles the user has
        /// and should be run after role changes
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<ReturnModel<bool>> SetLandingPage(AppUser user)
        {
            try
            {
                var activeRoles = await _userManager.GetRolesAsync(user);

                if (activeRoles.Contains(UserRoles.WorkflowAdmin))
                {
                    user.LandingPage = "WorkflowHistory";
                }
                else if (activeRoles.Contains(UserRoles.AdminRole))
                {
                    user.LandingPage = "UserManagement";
                }
                else
                {
                    user.LandingPage = "Dashboard";
                }
                
                var r = await _userManager.UpdateAsync(user);
                
                if (!r.Succeeded)
                {
                    return ReturnModel<bool>.ReturnError(
                        string.Join("\n", r.Errors.Select(e => $"{e.Code} - {e.Description}")));
                }


                return ReturnModel<bool>.ReturnSuccess(true);
            }
            catch (Exception e)
            {
                return ReturnModel<bool>.ReturnError(e);   
            }
        }

        /// <summary>
        /// remove a role from the user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public async Task<ReturnModel<bool>> RemoveRoleFromUser(Guid userId, string role)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId.ToString());
                var result = await _userManager.RemoveFromRoleAsync(user, role);
                
                if (!result.Succeeded)
                {
                    return ReturnModel<bool>.ReturnError(
                        string.Join("\n", result.Errors.Select(e => $"{e.Code} - {e.Description}")));
                }

                var r = await SetLandingPage(user);
                
                return r.IsSuccessful
                    ? ReturnModel<bool>.ReturnSuccess(true)
                    : ReturnModel<bool>.ReturnError(r.Errors);
            }
            catch (Exception e)
            {
                return ReturnModel<bool>.ReturnError(e);   
            }
        }

        /// <summary>
        /// adds a role to a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public async Task<ReturnModel<bool>> AddRoleToUser(Guid userId, string role)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId.ToString());
                var result = await _userManager.AddToRoleAsync(user, role);
                
                if (!result.Succeeded)
                {
                    return ReturnModel<bool>.ReturnError(
                        string.Join("\n", result.Errors.Select(e => $"{e.Code} - {e.Description}")));
                }

                var r = await SetLandingPage(user);
                
                return r.IsSuccessful
                    ? ReturnModel<bool>.ReturnSuccess(true)
                    : ReturnModel<bool>.ReturnError(r.Errors);
            }
            catch (Exception e)
            {
                return ReturnModel<bool>.ReturnError(e);   
            }
            
        }

      

        /// <summary>
        /// generate a JWT token for the user
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="identity"></param>
        /// <returns></returns>
        private async Task<string> GenerateEncodedToken(string userName, ClaimsIdentity identity)
        {
            //set issue time at now, the options seems to be cached hard in azure
            _jwtOptions.IssuedAt = DateTime.UtcNow;
            var user = await _userManager.FindByNameAsync(userName);
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, userName),
                new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(
                    _jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64), identity.FindFirst(Constants.JwtClaimIdentifiers.Rol),
                identity.FindFirst(Constants.JwtClaimIdentifiers.Id)
            };
            var userRoles = await _userManager.GetRolesAsync(user);
            
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
            }
            
            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                claims: claims,
                notBefore: _jwtOptions.NotBefore,
                expires: _jwtOptions.Expiration,
                signingCredentials: _jwtOptions.SigningCredentials);
            

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }
        
        /// <summary>
        /// validate user/password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);
            // get the user to verifty
            
            var userToVerify = _userManager.Users.FirstOrDefault(u => u.UserName.Equals(userName));

            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);
            // check the credentials  
            if (await _userManager.CheckPasswordAsync(userToVerify, password))
            {
                return await Task.FromResult(GenerateClaimsIdentity(userName, userToVerify.Id));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }
        
        /// <summary>
        /// generate identity
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private ClaimsIdentity GenerateClaimsIdentity(string userName, string id)
        {
            return new ClaimsIdentity(new GenericIdentity(userName, "Token"), new[]
            {
                new Claim(Constants.JwtClaimIdentifiers.Id, id),//todo get roles of the specific user
               // new Claim(Constants.JwtClaimIdentifiers.Rol, Constants.JwtClaims.ApiAccess)
            });
        }
        
        /// <returns>Date converted to seconds since Unix epoch (Jan 1, 1970, midnight UTC).</returns>
        private static long ToUnixEpochDate(DateTime date)
            => (long)Math.Round((date.ToUniversalTime() -
                                 new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero))
                .TotalSeconds);

        //private static 


    }
}
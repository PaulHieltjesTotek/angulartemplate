using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Attributes;
using Core.Models;
using Domain.Models;
using Domain.Models.Application;
using Infrastructure.Base;
using Infrastructure.Services.Interfaces;
using Persistence.Base;
using Persistence.Repositories.Interfaces;

namespace Infrastructure.Services
{
    [AutoDi(AddType.Transiant, typeof(IPickListService))]
    public class PickListService : BaseService<PickList>, IPickListService
    {
        private readonly IPickListRepository _repository;
        private readonly IRequestService _requestService;
        
        public PickListService(IPickListRepository repository, IRequestService request) : base(repository)
        {
            _repository = repository;
            _requestService = request;
        }

        /// <summary>
        /// reuturns the picklist options for a specific type.
        /// The display value is set based on the current language
        /// </summary>
        /// <param name="type"></param>
        /// <param name="includeInActive"></param>
        /// <returns></returns>
        public async Task<ReturnModel<IEnumerable<PickListViewModel>>> GetValuesForType(string type, bool includeInActive = true)
        {
            try
            {
                var values = await _repository.GetValuesForType(type, includeInActive);
                
                switch (_requestService.GetCurrentLanguage())
                {
                    case "nl":
                        return ReturnModel<IEnumerable<PickListViewModel>>.ReturnSuccess(
                            values.Select(v => new PickListViewModel
                            {
                               Id = v.Id,
                               Key = v.Key,
                               Active = v.Active,
                               DisplayOrder = v.DisplayOrder,
                               Value = v.NlValue
                            }).OrderBy(v =>v.DisplayOrder).ThenBy(v => v.Value));
                    case "fr":
                        return ReturnModel<IEnumerable<PickListViewModel>>.ReturnSuccess(
                            values.Select(v => new PickListViewModel
                            {
                                Id = v.Id,
                                Key = v.Key,
                                Active = v.Active,
                                DisplayOrder = v.DisplayOrder,
                                Value = v.FrValue
                            }).OrderBy(v =>v.DisplayOrder).ThenBy(v => v.Value));
                    default:
                        return ReturnModel<IEnumerable<PickListViewModel>>.ReturnSuccess(
                            values.Select(v => new PickListViewModel
                            {
                                Id = v.Id,
                                Key = v.Key,
                                Active = v.Active,
                                DisplayOrder = v.DisplayOrder,
                                Value = v.EnValue
                            }).OrderBy(v =>v.DisplayOrder).ThenBy(v => v.Value));
                }
            }
            catch (Exception e)
            {
                return ReturnModel<IEnumerable<PickListViewModel>>.ReturnError(e);
            }
            
        }
        
        /// <summary>
        /// Returns the raw values for a specific type. SO all language variants are there
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<ReturnModel<IEnumerable<PickList>>> GetRawValuesForType(string type)
        {
            try
            {
                var values = await _repository.GetValuesForType(type, true);
                return ReturnModel<IEnumerable<PickList>>.ReturnSuccess(values);
            }
            catch (Exception e)
            {
                return ReturnModel<IEnumerable<PickList>>.ReturnError(e);
            }
            
        }
        
        /// <summary>
        /// Returns all picklist types
        /// </summary>
        /// <returns></returns>
        public async Task<ReturnModel<IEnumerable<string>>> GetPickListTypes()
        {
            try
            {
                var values = await _repository.GetPicklistTypes();
                return ReturnModel<IEnumerable<string>>.ReturnSuccess(values);
            }
            catch (Exception e)
            {
                return ReturnModel<IEnumerable<string>>.ReturnError(e);
            }
            
        }
        
    }
}
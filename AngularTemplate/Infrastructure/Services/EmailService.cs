using System;
using System.Net.Mail;
using Core.Attributes;
using Infrastructure.Services.Interfaces;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.Services
{
	[AutoDi(AddType.Transiant, typeof(IEmailService))]
    public class EmailService : IEmailService
    {

        private readonly IRequestService _requestService;
        public EmailService(IRequestService requestService, IConfiguration configuration)
        {
	        _requestService = requestService;

        }
        
        
        private void SendEmail(string message, string subject, string to, string toName)
        {
	        SendOffice365Mail(message, subject, to, toName);
        }
        
        private static bool SendOffice365Mail(string mail, string subject, string to, string toName)
        {
	        MailMessage msg = new MailMessage();
	        msg.To.Add(new MailAddress(to, toName));
	        msg.From = new MailAddress("paulhieltjes@kiensystems.com", "Paul");
	        msg.Subject = subject;
	        msg.IsBodyHtml = true;
	        msg.Body = mail;

	        SmtpClient client = new SmtpClient();
	        client.UseDefaultCredentials = false;
	        client.Credentials = new System.Net.NetworkCredential("paulhieltjes@kiensystems.com", "SXuq5L44xIXl");
	        client.Port = 587; // You can use Port 25 if 587 is blocked
	        client.Host = "smtp.office365.com";
	        client.DeliveryMethod = SmtpDeliveryMethod.Network;
	        client.EnableSsl = true;
	        try
	        {
		        client.Send(msg);
	        }
	        catch (Exception ex)
	        {
		        Console.WriteLine(ex);
		        return false;
	        }
            
	        return true;
        }

       #region publics
       
       public void SendNewUser(string email, string username, string password)
       {

	       var message = $@"New user

<p>We have created a new user for you</p>

<p>The following instructions will help you to do so:
<ul>
    <li>Go to <a href='https://{_requestService.GetCurrentUrl()}/en/Login'>web application</a>.</li>
    <li>Username: {username}</li>
    <li>Password: {password}</li>                                
    <li>After initial login you will be prompted to change your password.</li>
</ul></p>
";


                    SendEmail(
	                    message, "Your user account has been created!", email,
       username);
       }

       public void SendResetPassword(string email, string password)
        {

            var message = $@"Reset your password

<p>We have reset your </p>

<p>The following instructions will help you to do so:
<ul>
    <li>Go to <a href='https://{_requestService.GetCurrentUrl()}/en/Login'>the online tool</a>.</li>
    <li>Password: {password}</li>                                
    <li>After initial login you will be prompted to change your password.</li>
</ul></p>
";


            SendEmail(
	            message, "Reset your password", email,
	            email);
        }
       
       #endregion
         
    }
}
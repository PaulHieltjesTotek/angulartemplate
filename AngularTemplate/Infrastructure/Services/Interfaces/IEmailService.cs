namespace Infrastructure.Services.Interfaces
{
    public interface IEmailService
    {
        void SendResetPassword(string email, string password);
        void SendNewUser(string email, string username, string password);
    }
}
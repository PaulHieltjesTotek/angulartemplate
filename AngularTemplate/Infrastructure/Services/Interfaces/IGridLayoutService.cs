using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;
using Domain.Models.Application;

namespace Infrastructure.Services.Interfaces
{
    public interface IGridLayoutService
    {
        Task<ReturnModel<IEnumerable<GridLayout>>> GetAllForGridAsync(string gridName);

        ReturnModel<IEnumerable<GridLayout>> GetAllForGrid(string gridName);

        Task<ReturnModel<GridLayout>> Post(GridLayout layout);

        Task<ReturnModel<GridLayout>> Put(GridLayout layout);
        
        ReturnModel<string> Delete(Guid id);
    }
}
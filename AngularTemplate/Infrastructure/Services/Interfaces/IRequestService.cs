using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Shared.Models.Authentication;

namespace Infrastructure.Services.Interfaces
{
    public interface IRequestService
    {
        string GetCurrentLanguage();
        string GetCurrentUserName();
        Guid GetCurrentUserId();
        AppUser GetCurrentIdentityUser();
        Task<IEnumerable<string>> GetCurrentRoles();

        string GetCurrentUrl();
        
    }
}
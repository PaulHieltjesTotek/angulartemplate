using System;
using System.IO;
using Core.Models;
using Domain.Models;
using Domain.Models.Application;
using Infrastructure.Base;
using Microsoft.AspNetCore.Http;
using Shared.ViewModels;

namespace Infrastructure.Services.Interfaces
{
    public interface IDocumentService : IBaseService<Document>
    {
        void CheckFileExtensionValid(string fileName);
        ReturnModel<string> UploadChunk(IFormFile file);
        (FileStream, string) DownloadDocument(Guid id);
        ReturnModel<string> SaveStream(Stream stream);
    }
}
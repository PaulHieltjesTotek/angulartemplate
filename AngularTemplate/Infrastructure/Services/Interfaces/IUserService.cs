using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;
using Shared.Models.Authentication;
using Shared.ViewModels;

namespace Infrastructure.Services.Interfaces
{
    public interface IUserService
    {
        Task<ReturnModel<Guid>> RegisterUser(RegistrationViewModel model);

        Task<ReturnModel<string>> Login(CredentialsViewModel model);
        
        Task<ReturnModel<IEnumerable<string>>> GetRolesForUser(Guid userId);
        
        Task<ReturnModel<IEnumerable<string>>> GetMissingRolesForUser(Guid userId);
        
        Task<ReturnModel<bool>> RemoveRoleFromUser(Guid userId, string role);
        
        Task<ReturnModel<bool>> AddRoleToUser(Guid userId, string role);
        
        Task<ReturnModel<bool>> CreateUser(UserCreationViewModel user);
        Task<ReturnModel<(bool existing, AppUser user, string password)>> ActivateUser(Guid id, string langingPage = "Dashboard");
        Task<ReturnModel<bool>> ResetPassword(string username);
        Task<ReturnModel<bool>> ChangePassword(ChangePasswordViewModel model);
        Task<ReturnModel<bool>> SetActivenessUser(Guid id, bool active);
        Task<ReturnModel<bool>> DeleteUser(Guid id);
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models;
using Domain.Models;
using Domain.Models.Application;
using Infrastructure.Base;

namespace Infrastructure.Services.Interfaces
{
    public interface IPickListService : IBaseService<PickList>
    {
        Task<ReturnModel<IEnumerable<PickListViewModel>>> GetValuesForType(string type, bool includeInActive = true);
        Task<ReturnModel<IEnumerable<PickList>>> GetRawValuesForType(string type);
        Task<ReturnModel<IEnumerable<string>>> GetPickListTypes();
    }
}
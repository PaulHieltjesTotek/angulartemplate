using Domain.Base;

namespace Infrastructure.Services.Interfaces
{
    public interface IWfService
    {
        bool IsCurrentAssignee(BaseWorkflowModel wfModel);
        bool IsRequester(BaseWorkflowModel wfModel);
    }
}
using System;
using System.Linq;
using Core.Attributes;
using Domain.Base;
using Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Shared.Models.Authentication;

namespace Infrastructure.Services
{
    [AutoDi(AddType.Transiant, typeof(IWfService))]
    public class WfService : IWfService
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IRequestService _requestService;
        
        public WfService(UserManager<AppUser> userManager, IRequestService requestService)
        {
            _userManager = userManager;
            _requestService = requestService;
        }

        /// <summary>
        /// checks if the current user is the assignee, if current assignee is a role then
        /// this function checks if the current user has the required role
        /// </summary>
        /// <param name="wfModel"></param>
        /// <returns></returns>
        public bool IsCurrentAssignee(BaseWorkflowModel wfModel)
        {

            if (wfModel.IsAssigneeUser)
            {
                return string.Equals(wfModel.Assignee, _requestService.GetCurrentUserName(),
                    StringComparison.InvariantCultureIgnoreCase);
            }
            
            var user = _userManager.Users.FirstOrDefault(u => 
                u.UserName.Equals(_requestService.GetCurrentUserName(), StringComparison.InvariantCultureIgnoreCase));
            
            return _userManager.GetRolesAsync(user).Result.Contains(wfModel.Assignee);

        }

        /// <summary>
        /// CHecks if the current user is the requester of this workflow model
        /// </summary>
        /// <param name="wfModel"></param>
        /// <returns></returns>
        public bool IsRequester(BaseWorkflowModel wfModel)
        {
            return wfModel.Requester.Equals(_requestService.GetCurrentUserName(), StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
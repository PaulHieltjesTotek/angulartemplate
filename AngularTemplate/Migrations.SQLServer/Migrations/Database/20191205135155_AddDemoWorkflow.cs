﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Migrations.SQLServer.Migrations.Database
{
    public partial class AddDemoWorkflow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_DemoWorkflowModel",
                table: "DemoWorkflowModel");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Audit",
                table: "Audit");

            migrationBuilder.RenameTable(
                name: "DemoWorkflowModel",
                newName: "Main_DemoWorkflowModel");

            migrationBuilder.RenameTable(
                name: "Audit",
                newName: "Main_Audit");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Main_DemoWorkflowModel",
                table: "Main_DemoWorkflowModel",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Main_Audit",
                table: "Main_Audit",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Main_DemoWorkflowModel",
                table: "Main_DemoWorkflowModel");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Main_Audit",
                table: "Main_Audit");

            migrationBuilder.RenameTable(
                name: "Main_DemoWorkflowModel",
                newName: "DemoWorkflowModel");

            migrationBuilder.RenameTable(
                name: "Main_Audit",
                newName: "Audit");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DemoWorkflowModel",
                table: "DemoWorkflowModel",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Audit",
                table: "Audit",
                column: "Id");
        }
    }
}

﻿// <auto-generated />
using System;
using DbContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Migrations.SQLServer.Migrations.Application
{
    [DbContext(typeof(ApplicationContext))]
    partial class ApplicationContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.0-rtm-35687")
                .HasAnnotation("PropertyAccessMode", PropertyAccessMode.Property)
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Domain.Models.Application.Document", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt");

                    b.Property<Guid?>("CreatedBy");

                    b.Property<string>("FileExtension");

                    b.Property<string>("FileId");

                    b.Property<string>("FileName");

                    b.Property<Guid>("GroupingId");

                    b.Property<DateTime>("LastUpdatedAt");

                    b.Property<Guid?>("LastUpdatedBy");

                    b.Property<Guid>("ParentObjectId");

                    b.HasKey("Id");

                    b.ToTable("Application_Document");
                });

            modelBuilder.Entity("Domain.Models.Application.GridLayout", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("GridId")
                        .HasColumnType("varchar(64)")
                        .HasMaxLength(75);

                    b.Property<string>("Layout")
                        .HasColumnType("varchar(4000)")
                        .HasMaxLength(5000);

                    b.Property<string>("LayoutName")
                        .HasColumnType("varchar(64)")
                        .HasMaxLength(75);

                    b.Property<Guid?>("UserId");

                    b.HasKey("Id");

                    b.ToTable("Application_GridLayout");
                });

            modelBuilder.Entity("Domain.Models.Application.PickList", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<Guid?>("CreatedBy");

                    b.Property<int>("DisplayOrder");

                    b.Property<string>("EnValue");

                    b.Property<string>("FrValue");

                    b.Property<string>("Key");

                    b.Property<DateTime>("LastUpdatedAt");

                    b.Property<Guid?>("LastUpdatedBy");

                    b.Property<string>("NlValue");

                    b.Property<string>("Type");

                    b.HasKey("Id");

                    b.ToTable("Application_PickList");
                });

            modelBuilder.Entity("Domain.Models.Auditing.Audit", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateTime");

                    b.Property<string>("KeyValues");

                    b.Property<string>("NewValues");

                    b.Property<string>("OldValues");

                    b.Property<string>("TableName");

                    b.HasKey("Id");

                    b.ToTable("Application_Audit");
                });
#pragma warning restore 612, 618
        }
    }
}

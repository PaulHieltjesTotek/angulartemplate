﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Migrations.SQLServer.Migrations.Application
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GridLayout",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GridId = table.Column<string>(type: "varchar(64)", maxLength: 75, nullable: true),
                    LayoutName = table.Column<string>(type: "varchar(64)", maxLength: 75, nullable: true),
                    Layout = table.Column<string>(type: "varchar(4000)", maxLength: 5000, nullable: true),
                    UserId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GridLayout", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GridLayout");
        }
    }
}

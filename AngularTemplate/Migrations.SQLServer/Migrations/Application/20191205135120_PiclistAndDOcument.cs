﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Migrations.SQLServer.Migrations.Application
{
    public partial class PiclistAndDOcument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_GridLayout",
                table: "GridLayout");

            migrationBuilder.RenameTable(
                name: "GridLayout",
                newName: "Application_GridLayout");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Application_GridLayout",
                table: "Application_GridLayout",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Application_Audit",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TableName = table.Column<string>(nullable: true),
                    DateTime = table.Column<DateTime>(nullable: false),
                    KeyValues = table.Column<string>(nullable: true),
                    OldValues = table.Column<string>(nullable: true),
                    NewValues = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Application_Audit", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Application_Document",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    LastUpdatedAt = table.Column<DateTime>(nullable: false),
                    LastUpdatedBy = table.Column<Guid>(nullable: true),
                    FileId = table.Column<string>(nullable: true),
                    FileExtension = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    ParentObjectId = table.Column<Guid>(nullable: false),
                    GroupingId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Application_Document", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Application_PickList",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<Guid>(nullable: true),
                    LastUpdatedAt = table.Column<DateTime>(nullable: false),
                    LastUpdatedBy = table.Column<Guid>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Key = table.Column<string>(nullable: true),
                    EnValue = table.Column<string>(nullable: true),
                    NlValue = table.Column<string>(nullable: true),
                    FrValue = table.Column<string>(nullable: true),
                    DisplayOrder = table.Column<int>(nullable: false),
                    Active = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Application_PickList", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Application_Audit");

            migrationBuilder.DropTable(
                name: "Application_Document");

            migrationBuilder.DropTable(
                name: "Application_PickList");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Application_GridLayout",
                table: "Application_GridLayout");

            migrationBuilder.RenameTable(
                name: "Application_GridLayout",
                newName: "GridLayout");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GridLayout",
                table: "GridLayout",
                column: "Id");
        }
    }
}

using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;

namespace Core.Helpers
{
    public class ApiHelper
    {
        private HttpClient _client = new HttpClient();
        private string _apiUrl;


        public ApiHelper(string apiUrl) 
        {
            _apiUrl = apiUrl;
        }

        public T Post<T>(string path, object data)
        {
            var myContent = JsonConvert.SerializeObject(data);
            var buffer = Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = _client.PostAsync($"{_apiUrl}{path}", byteContent).Result;

            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<T>(result);
            }

            throw new Exception("Request failed: " + response.Content.ReadAsStringAsync().Result);
        }

        public T Get<T>(string path)
        {
            var response = _client.GetAsync($"{_apiUrl}{path}").Result;

            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<T>(result);
            }

            throw new Exception("Request failed: " + response.Content.ReadAsStringAsync().Result);
        }

        public string Delete(string path)
        {
            var response = _client.DeleteAsync($"{_apiUrl}{path}").Result;

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsStringAsync().Result;
            }

            throw new Exception("Request failed: " + response.Content.ReadAsStringAsync().Result);
        }

        public T Delete<T>(string path)
        {
            var response = _client.DeleteAsync($"{_apiUrl}{path}").Result;

            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<T>(result);
            }

            throw new Exception("Request failed: " + response.Content.ReadAsStringAsync().Result);
        }
    }
}
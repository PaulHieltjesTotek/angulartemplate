﻿using System.Collections.Generic;
using System.IO;

namespace Core.Helpers
{
    public class FileHelper
    {
        public IEnumerable<string> GetFilesFromDirectory(string path, string[] fileTypes)
        {
            string[] files = Directory.GetFiles(path);
            return FilterFiles(files, fileTypes);
        }

        public IEnumerable<string> FilterFiles(string[] fileNames, string[] fileTypes)
        {
            foreach (var file in fileNames)
            {
                var fileExtension = Path.GetExtension(file);

                foreach (string fileType in fileTypes)
                    if (fileExtension != null && fileExtension.Contains(fileType))
                    {
                        yield return Path.GetFileName(file);
                    }
            }
        }

        public string GetExtensionWithoutDot(string fileName)
        {
            return Path.GetExtension(fileName)?.Replace(".", "");
        }
    }
}

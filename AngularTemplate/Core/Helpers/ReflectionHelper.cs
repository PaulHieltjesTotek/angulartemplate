﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Core.Helpers
{
    public class ReflectionHelper
    {
        public IEnumerable<Type> GetAllClassesThatInheritFromInterface(Type interfaceType, string assemblyString)
        {
            Assembly.Load(assemblyString);
            var type = interfaceType;
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface);
            return types;
        }
    }
}

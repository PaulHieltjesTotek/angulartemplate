﻿using System;

namespace Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ConfiguratorStringToEmptyIf0Attribute : Attribute
    {
    }
}

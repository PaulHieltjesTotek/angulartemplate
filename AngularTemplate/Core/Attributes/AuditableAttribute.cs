using System;

namespace Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class AuditableAttribute: Attribute
    {
        
    }
}
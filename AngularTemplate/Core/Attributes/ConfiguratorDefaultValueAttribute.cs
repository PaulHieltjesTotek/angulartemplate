﻿using System;

namespace Core.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ConfiguratorDefaultValueAttribute : Attribute
    {
        public object DefaultValue { get; set; }

        public ConfiguratorDefaultValueAttribute(object defaultValue)
        {
            DefaultValue = defaultValue;
        }
    }
}

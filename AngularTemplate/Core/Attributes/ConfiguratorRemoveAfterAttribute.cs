﻿using System;

namespace Core.Attributes
{
	[AttributeUsage(AttributeTargets.Property)]
	public class ConfiguratorRemoveAfterAttribute : Attribute
	{
		public string RemoveText { get; set; }

		public ConfiguratorRemoveAfterAttribute(string removeText)
		{
			RemoveText = removeText;
		}
	}
}

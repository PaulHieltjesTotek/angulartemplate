using System;

namespace Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class AutoDi: Attribute
    {
        public Type InterfaceType { get; set; }
        public AddType AddType { get; set; }
        
        
        public AutoDi(AddType addType, Type interfaceType = null)
        {
            InterfaceType = interfaceType;
            AddType = addType;
        }
    }

    public enum AddType
    {
        Scoped,
        Transiant,
        Singleton
    }
}
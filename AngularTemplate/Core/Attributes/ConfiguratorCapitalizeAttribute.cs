﻿using System;

namespace Core.Attributes
{
	[AttributeUsage(AttributeTargets.Property)]
	public class ConfiguratorCapitalizeAttribute : Attribute
	{
	}
}

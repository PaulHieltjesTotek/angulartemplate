﻿using System;

namespace Core.Attributes
{
	[AttributeUsage(AttributeTargets.Property)]
	public class ConfiguratorRemoveSpecialAttribute : Attribute
	{
		public char[] Except { get; set; }

		public ConfiguratorRemoveSpecialAttribute(params char[] except)
		{
			Except = except;
		}
	}
}

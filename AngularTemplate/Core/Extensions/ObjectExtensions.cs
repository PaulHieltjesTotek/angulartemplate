using System;
using System.Globalization;
using Core.Attributes;

namespace Core.Extensions
{
    public static class ObjectExtensions
    {
        public static bool IsAuditable(this object obj)
        {
            return Attribute.GetCustomAttribute(obj.GetType(), typeof(AuditableAttribute)) != null;
        }

        public static string ToInvariantString(this object obj)
        {
            return Convert.ToString(obj, CultureInfo.InvariantCulture);
        }       

    }
}
using System;

namespace Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool Between(this DateTime value, DateTime from, DateTime to)
        {
            return value >= from && value <= to;
        }

        public static DateTime GmtToCentralEuropeanStandardTime(DateTime dateTime)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime,
                TimeZoneInfo.FindSystemTimeZoneById("Central Europe Standard Time"));
        }
    }
}
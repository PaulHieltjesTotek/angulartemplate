﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Core.Extensions
{
    public static class StringExtensions
    {
     	public static string ConvertToEmptyIf0(this string str)
        {
            if(str == "0" || str =="\0")
                return String.Empty;
            else
            {
                return str;
            }
        }

        public static void CleanupFileFolder(this string folderPath, TimeSpan time)
        {
            foreach (var file in Directory.GetFiles(folderPath))
            {
                if (DateTime.Now - File.GetCreationTime(file) > time)
                {
                    File.Delete(file);
                }
            }
            
            foreach (var dir in Directory.GetDirectories(folderPath))
            {
                if (DateTime.Now - Directory.GetCreationTime(dir) > time)
                {
                    Directory.Delete(dir, true);
                }
            }
        }
        
        public static int GetMinutesFromHHMM(this string str)
        {
            var result = str.PadLeft(4, '0');         
            var minutes = Convert.ToInt32(result.Substring(2, 2));
            var hours = Convert.ToInt32(result.Substring(0, 2));
            return minutes + (hours * 60);
        }

        public static string ExtractHoursAndMinutesFromYYHHMM(this string str)
        {
            var result = str.PadLeft(6, '0'); //091206
            return result.Substring(2, 4);
        }


        public static IEnumerable<int> AllIndexesOf(this string str, string value)
        {
            if (String.IsNullOrEmpty(value))
                throw new ArgumentException("the string to find may not be empty", "value");
            for (int index = 0; ; index += value.Length)
            {
                index = str.IndexOf(value, index);
                if (index == -1)
                    break;
                yield return index;
            }
        }

        public static bool OnlyContainsAlphanumerical(this string s)
        {
            return Regex.IsMatch(s, @"^[a-zA-Z0-9]+$");
        }

        public static bool MatchesYYMMDD(this string s)
        {
	        return DateTime.TryParseExact(
		        s,
		        "yyMMdd",
		        CultureInfo.InvariantCulture,
		        DateTimeStyles.AdjustToUniversal,
		        out var result);
        }

        public static bool MatchesYYMMDDOrNull(this string s)
        {
            if (String.IsNullOrEmpty(s))
                return true;

            return MatchesYYMMDD(s);
        }

        public static bool OnlyHexInString(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return true;
            return Regex.IsMatch(s, @"\A\b[0-9a-fA-F]+\b\Z");
        }

        public static bool MatchesDateTimeTextFormat(this string s)
        {
            return Regex.IsMatch(s, "^(\\d\\d)(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])$");
        }
        /// <summary>
        /// Removes all whitespaces from a string
        /// </summary>
        /// <param name="s">string to process</param>
        /// <returns></returns>
        public static string RemoveWhitespaces(this string s)
        {
            return s.Replace(" ", "").Replace("\t", "").Replace("\n", "").Replace("\r", "");
        }

        /// <summary>
        /// Returns the first element after splitting the string a given character
        /// </summary>
        /// <param name="s">string to process</param>
        /// <returns></returns>
        public static string ReturnFirstSplit(this string s, char charToSplitOn)
        {
            return s.Split(charToSplitOn).First();
        }

        /// <summary>
        /// Regex method for checking if a string matches the HHMM format
        /// </summary>
        /// <param name="s"></param>
        /// <param name="nullIsValid"></param>
        /// <returns></returns>
        public static bool MatchesHHMM(this string s)
        {
            return MatchesHHMM(s, false);
        }

        /// <summary>
        /// Regex method for checking if a string matches the HHMM format
        /// </summary>
        /// <param name="s"></param>
        /// <param name="nullIsValid"></param>
        /// <returns></returns>
        public static bool MatchesHHMM(this string s, bool nullIsValid)
        {
            if(nullIsValid)
                if (String.IsNullOrEmpty(s))
                    return true;

	        return TimeSpan.TryParse(s, out var result);
        }


        /// <summary>
        /// Regex method for checking if a string matches the HHMM format
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool MatchesHHMMOrNull(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return true;

            return MatchesHHMM(s);
        }


        /// <summary>
        /// Regex method for checking if a string matches the DDHHMM format
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool MatchesDDHHMMRegex(this string s)
        {
            var regexBody = $"^[0-3][0-9][0-2][0-9][0-5][0-9]$";
            return Regex.IsMatch(s, regexBody);
        }


        /// <summary>
        /// Regex method for checking if a string matches the DDHHMM format
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool MatchesDDHHMM(this string s)
        {
            try
            {
                string format = "ddHHmm";
                DateTime r = DateTime.ParseExact(s, format, CultureInfo.InvariantCulture);
                return true;
            }
            catch
            {
                return false;
            }          

        }

        /// <summary>
        /// Method for checking if a string only contains letters
        /// </summary>
        /// <param name="s">String to process</param>
        /// <returns></returns>
        public static bool OnlyContainsLetters(this string s)
        {
            if(!String.IsNullOrEmpty(s))
            return Regex.IsMatch(s, @"^[a-zA-Z]+$");
            return true;
        }

        /// <summary>
        /// Method for checking if a string only contains CAPITAL letters
        /// </summary>
        /// <param name="s">String to process</param>
        /// <returns></returns>
        public static bool OnlyContainsCapitalLetters(this string s)
        {
            if (!String.IsNullOrEmpty(s))
                return Regex.IsMatch(s, @"^[A-Z]+$");
            return true;
        }

        public static string ToUpper(this string s)
        {
            if (!string.IsNullOrEmpty(s))
                return s.ToUpper();
            else return null;
        }


        /// <summary>
        /// Removes all special characters except the ones given along with method
        /// </summary>
        /// <param name="s">string to process</param>
        /// <param name="except">char array of characters to keep (apart from Alphabetical)</param>
        /// <returns></returns>
        public static string RemoveAllSpecialCharacters(this string s, params char[] except)
        {
            var charset = Regex.Escape(new string(except));         
            var regexBody = $"[^0-9a-zA-Z{charset}]+";
            s = Regex.Replace(s, regexBody, string.Empty);
            return s;
        }

        public static string Clean(this string s)
        {
            if (!string.IsNullOrEmpty(s))
                return s.Trim().RemoveWhitespaces().RemoveAllSpecialCharacters();
            else return null;
        }

        public static bool IsCertainLetter(this string s, params char[] contains)
        {
            var charset = Regex.Escape(new string(contains));

            return Regex.IsMatch(s, $"[{charset}]");
        }

        public static bool IsCertainLetterOrEmpty(this string s, params char[] contains)
        {
            if (String.IsNullOrEmpty(s))
                return true;
            else
            {
                return IsCertainLetter(s, contains);
            }
        }

        public static bool IsCertainLetter(this char s, params char[] contains)
        {
            string value = s.ToString();
            var charset = Regex.Escape(new string(contains));
            string str = s.ToString();
            return Regex.IsMatch(str, $"[{charset}]");
        }

        public static bool IsCertainLetter2(char s, params char[] contains)
        {
            string value = s.ToString();
            var charset = Regex.Escape(new string(contains));
            string str = s.ToString();
            return Regex.IsMatch(str, $"[{charset}]");
        }

        public static bool HasLetter(this string s)
        {
            return Regex.IsMatch(s, "[A-Z]");
        }

        public static bool IsNumberBetween(this string s, int n1, int n2)
        {
	        if (!int.TryParse(s, out var n)) return false;

	        return n > 0 && n < 20;
        }

        public static bool IsANumber(this string s)
        {
            return decimal.TryParse(s, out var r);
        }
        
        public static bool IsNotANumber(this string s)
        {
            return !decimal.TryParse(s, out var r);
        }

        public static string RemoveNumbersRight(this string s)
        {
            var pattern = @"\d+$";
            var replacement = "";
            var rgx = new Regex(pattern);
            return rgx.Replace(s, replacement);
        }
    }
}

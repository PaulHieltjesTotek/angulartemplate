﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Core.Models
{
	public class ReturnModel<T>
	{
		public static ReturnModel<T> ReturnSuccess(T result)
		{
			return new ReturnModel<T> {IsSuccessful = true, Result = result};
		}
		
		public static ReturnModel<T> ReturnSuccess()
		{
			return new ReturnModel<T> {IsSuccessful = true, Result = default(T)};
		}

		public static ReturnModel<T> ReturnError(ModelStateDictionary modelState)
		{
			return new ReturnModel<T> {IsSuccessful = false, 
				Errors = modelState.Values.SelectMany(m => m.Errors)
				.Select(e => e.ErrorMessage)
				.ToList()};
		}
		
		public static ReturnModel<T> ReturnError(Exception e)
		{
			return new ReturnModel<T> {IsSuccessful = false, 
				Errors = new [] { e.Message } };
		}
		
		public static ReturnModel<T> ReturnError(string message)
		{
			return new ReturnModel<T> {IsSuccessful = false, Errors = new [] { message }};
		}
		
		public static ReturnModel<T> ReturnError(IEnumerable<string> errors)
		{
			return new ReturnModel<T> {IsSuccessful = false, Errors = errors};
		}

		public T Result { get; set; }

		public bool IsSuccessful { get; set; }

		public IEnumerable<string> Errors { get; set; }

		public string ErrorsAsString => Errors == null ? string.Empty : string.Join(" - ", Errors);
	}
}

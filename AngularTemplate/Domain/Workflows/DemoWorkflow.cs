using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Base;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Shared.Models.Authentication;
using Shared.ViewModels;
using tomware.Microwf.Core.Definition;

namespace Domain.Workflows
{
    public class DemoWorkflow : BaseWorkflow
    {
        public const string TYPE = "DemoWorkflow";
        
        public const string TRIGGER_APPLICANTION_APPROVED = "TRIGGER_APPLICANTION_APPROVED";
        public const string TRIGGER_APPLICATION_CANCELLED = "TRIGGER_APPLICATION_CANCELLED";

        public const string STATE_NEW= "APP_NEW";
        public const string STATE_APPROVED = "APP_APPROVED";
        public const string STATE_CANCELLED = "APP_CANCELLED";

        public DemoWorkflow(ILogger<DemoWorkflow> logger, IHttpContextAccessor accessor, 
            UserManager<AppUser> userManager) : base(logger, accessor, userManager)
        {
        }

        public override string Type => TYPE;
        public override List<Transition> Transitions => new List<Transition>
        {
            new Transition {
                State = STATE_NEW,
                Trigger = TRIGGER_APPLICANTION_APPROVED,
                TargetState = STATE_APPROVED,
                CanMakeTransition = CanMakeTransition,
                AfterTransition = AssignToNextStep,
                SendBackToRequester = true
            },
            new Transition {
                State = STATE_NEW,
                Trigger = TRIGGER_APPLICATION_CANCELLED,
                TargetState = STATE_CANCELLED,
                CanMakeTransition = CanMakeTransition,
                AfterTransition = AssignToNextStep,
                SendBackToRequester = true
            },
        };
        public override Type EntityType => typeof(DemoWorkflowModel);

    }
}
using System.Collections.Generic;
using tomware.Microwf.AspNetCoreEngine.Common;

namespace Domain
{
    public static class UserRoles
    {
        public const string AdminRole = "admin";
        public const string WorkflowAdmin = Constants.WORKFLOW_ADMIN_ROLE;
    }
}
using System;

namespace Domain.Base
{
    public abstract class BaseTrackableModel : BaseConfigurator
    {    
        public DateTime CreatedAt { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public Guid? LastUpdatedBy { get; set; }
    }
}
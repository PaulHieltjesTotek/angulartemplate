using System;
using System.ComponentModel.DataAnnotations;

namespace Domain.Base
{
    public abstract class BaseModel
    {
        
        [Key]
        public virtual Guid Id { get; set; }
    }
}
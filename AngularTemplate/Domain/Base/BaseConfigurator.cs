﻿using System.Linq;
using Core.Attributes;
using Core.Extensions;

namespace Domain.Base
{
    public abstract class BaseConfigurator : BaseModel
    {
        public void Configurate()
        {
            var props = GetType().GetProperties();

            foreach (var prop in props.Where(p => p.PropertyType == typeof(string)))
            {
                var attrs = prop.GetCustomAttributes(true);
                foreach (var attr in attrs)
                {
                    var value = prop.GetValue(this)?.ToString();
                    if (string.IsNullOrEmpty(value))
                    {
                        switch (attr)
                        {
                            case ConfiguratorDefaultValueAttribute defaultValue:
                                prop.SetValue(this, defaultValue.DefaultValue.ToString());
                                continue;
                            default: continue;
                        }
                    }

                    switch (attr)
                    {
                        case ConfiguratorTrimAttribute trim:
                            prop.SetValue(this, value.Trim());
                            continue;
                        case ConfiguratorCapitalizeAttribute cap:
                            prop.SetValue(this, value.ToUpper());
                            continue;
                        case ConfiguratorRemoveAfterAttribute removeAfter:
                            var index = value.IndexOf(removeAfter.RemoveText);
                            if (index > -1)
                            {
                                prop.SetValue(this, value.Remove(index));
                            }
                            continue;
                        case ConfiguratorRemoveSpacesAttribute removeSpaces:
                            prop.SetValue(this, value.RemoveWhitespaces());
                            continue;
                        case ConfiguratorRemoveSpecialAttribute removeSpecial:
                            prop.SetValue(this, value.RemoveAllSpecialCharacters(removeSpecial.Except));
                            continue;
                        case ConfiguratorStringToEmptyIf0Attribute toEmptyIf0:
                            prop.SetValue(this, value.ConvertToEmptyIf0());
                            continue;
                        default:
                            continue;
                    }
                }
            }

        }
    }
}

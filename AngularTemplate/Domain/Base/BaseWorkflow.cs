using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Shared.Models.Authentication;
using tomware.Microwf.AspNetCoreEngine.Workflows;
using tomware.Microwf.Core.Execution;

namespace Domain.Base
{
    public abstract class BaseWorkflow : EntityWorkflowDefinitionBase
    {
        protected readonly ILogger<BaseWorkflow> _logger;
        protected readonly IHttpContextAccessor _accessor;
        protected readonly UserManager<AppUser> _userManager;
        
        protected BaseWorkflow(ILogger<BaseWorkflow> logger, IHttpContextAccessor accessor, 
            UserManager<AppUser> userManager)
        {
            _logger = logger;
            _accessor = accessor;
            _userManager = userManager;
        }
        
        protected void AssignToNextStep(TransitionContext context)
        {
            var wfModel = context.GetInstance<BaseWorkflowModel>();

            var variable = context.GetFirstVariable();
            if (variable != null)
            {
                wfModel.Message = variable.Message;
            }
            
            if (context.Transition.SendBackToRequester)
            {
                wfModel.Assignee = wfModel.Requester;
                wfModel.IsAssigneeUser = true;
            }
            else if (context.Transition.SetUserAsHandler)
            {
                wfModel.Assignee = _userManager.GetUserId(_accessor.HttpContext.User);
                wfModel.Handler = _userManager.GetUserId(_accessor.HttpContext.User);
                wfModel.IsAssigneeUser = true;
            }
            else if (context.Transition.SendBackToHandler)
            {
                wfModel.Assignee = wfModel.Handler;
                wfModel.IsAssigneeUser = true;
            }
            else if (!string.IsNullOrWhiteSpace(context.Transition.Assignee))
            {
                wfModel.Assignee = context.Transition.Assignee;
                wfModel.IsAssigneeUser = context.Transition.IsAssigneeUser;
            }
            else
            {
                wfModel.Assignee = variable.Assignee;
                wfModel.IsAssigneeUser = variable.IsAssigneeUser;
            }
        }
        
        protected bool CanMakeTransition(TransitionContext context)
        {
            var wfModel = context.GetInstance<BaseWorkflowModel>();

            _logger.LogInformation($"WF entity in CanMakeTransition: {wfModel.Assignee}");
            

            if (wfModel.IsAssigneeUser)
            {
                return string.Equals(wfModel.Assignee, _userManager.GetUserId(_accessor.HttpContext.User), StringComparison.InvariantCultureIgnoreCase);
            }
            var user = _userManager.Users.FirstOrDefault(u => 
                u.UserName.Equals(_userManager.GetUserId(_accessor.HttpContext.User), StringComparison.InvariantCultureIgnoreCase));
            
            return _userManager.GetRolesAsync(user).Result.Contains(wfModel.Assignee);

        }
        
        protected bool IsUserRequester(TransitionContext context)
        {
            var wfModel = context.GetInstance<BaseWorkflowModel>();
            
            _logger.LogInformation($"WF entity in IsUserRequester assignee: {wfModel.Assignee} requester: {wfModel.Requester}");

            return string.Equals(wfModel.Requester, _userManager.GetUserId(_accessor.HttpContext.User),
                StringComparison.InvariantCultureIgnoreCase);

        }
        
        
        protected bool IsUserRequesterOrCreator(TransitionContext context)
        {
            var wfModel = context.GetInstance<BaseWorkflowModel>();
            
            _logger.LogInformation($"WF entity in IsUserRequester assignee: {wfModel.Assignee} requester: {wfModel.Requester}");
            var username = _userManager.GetUserId(_accessor.HttpContext.User)?.ToLower();
            var result = wfModel.Requester.Equals(username, StringComparison.InvariantCultureIgnoreCase);

            if (result)
            {
                return true;
            }

            var user = _userManager.Users.FirstOrDefault(u =>
                u.UserName.Equals(username, StringComparison.InvariantCultureIgnoreCase));

            if (user == null)
            {
                return false;
            }

            if (Guid.TryParse(user.Id, out var id))
            {
                return id == wfModel.CreatedBy;
            }

            return false;

        }
    }
}
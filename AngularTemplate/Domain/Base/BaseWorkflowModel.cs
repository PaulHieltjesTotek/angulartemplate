using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using tomware.Microwf.AspNetCoreEngine.Workflows;

namespace Domain.Base
{
    public class BaseWorkflowModel : BaseTrackableModel, IEntityWorkflow
    {
        public string Type { get; set; }
        public string State { get; set; }
        
        public bool IsAssigneeUser { get; set; }
        public string Assignee { get; set; }
        
        public string Requester { get; set; }
        
        public string Handler { get; set; }
        
        [NotMapped]
        public string Message { get; set; }
    }
}
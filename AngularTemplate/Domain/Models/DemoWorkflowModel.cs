using Domain.Base;
using Domain.Workflows;

namespace Domain.Models
{
    public class DemoWorkflowModel : BaseWorkflowModel
    {
        public DemoWorkflowModel()
        {
            Type = DemoWorkflow.TYPE;
            State = DemoWorkflow.STATE_NEW;
        }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
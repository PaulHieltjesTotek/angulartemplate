using System;
using Core.Attributes;
using Domain.Base;

namespace Domain.Models.Application
{
    [Auditable]
    public class Document : BaseTrackableModel
    {
        public string FileId { get; set; }
        public string FileExtension { get; set; }
        public string FileName { get; set; }

        public Guid ParentObjectId { get; set; }
        
        public Guid GroupingId { get; set; }
    }
}
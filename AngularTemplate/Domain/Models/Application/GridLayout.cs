using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Base;

namespace Domain.Models.Application
{
    public class GridLayout : BaseModel
    {
        
        [Column(TypeName = ("varchar(64)")), MaxLength(75)]
        public string GridId { get; set; }
        
        [Column(TypeName = ("varchar(64)")), MaxLength(75)]
        public string LayoutName { get; set; }
        
        [Column(TypeName = ("varchar(4000)")), MaxLength(5000)]
        public string Layout { get; set; }
        
        public Guid? UserId { get; set; } 
    }
}
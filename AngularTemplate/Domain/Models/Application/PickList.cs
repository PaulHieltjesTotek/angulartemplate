using Core.Attributes;
using Domain.Base;

namespace Domain.Models.Application
{
    [Auditable]
    public class PickList : BaseTrackableModel
    {
        public PickList()
        {
        }
        
        public PickList(string type, string key, string value)
        {
            Type = type;
            Key = key;
            EnValue = value;
            NlValue = value;
            FrValue = value;
            Active = true;
            DisplayOrder = 9999;
        }
        
        public PickList(string type, string key, string enValue, string nlValue, string frValue)
        {
            Type = type;
            Key = key;
            EnValue = enValue;
            NlValue = nlValue;
            FrValue = frValue;
            Active = true;
            DisplayOrder = 9999;
        }
        
        public PickList(string type, string key, string enValue, string nlValue, string frValue, int displayOrder)
        {
            Type = type;
            Key = key;
            EnValue = enValue;
            NlValue = nlValue;
            FrValue = frValue;
            Active = true;
            DisplayOrder = displayOrder;
        }
        
        public string Type { get; set; }
        public string Key { get; set; }
        public string EnValue { get; set; }
        public string NlValue { get; set; }
        public string FrValue { get; set; }
        
        public int DisplayOrder { get; set; }
        public bool Active { get; set; } = true;
    }

    public class PickListViewModel : BaseModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int? DisplayOrder { get; set; }
        public bool Active { get; set; }
    }
}
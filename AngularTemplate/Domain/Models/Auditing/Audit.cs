using System;
using System.ComponentModel.DataAnnotations;
using Domain.Base;

namespace Domain.Models.Auditing
{
    public class Audit : BaseModel
    {
        public string TableName { get; set; }
        public DateTime DateTime { get; set; }
        public string KeyValues { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }
    }

}
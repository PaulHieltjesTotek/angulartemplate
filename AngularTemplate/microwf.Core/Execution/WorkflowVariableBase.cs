using System;
using System.Collections.Generic;

namespace tomware.Microwf.Core.Execution
{
  public class WorkflowVariableBase
  {
    public string Message { get; set; }
    
    public string Assignee { get; set; }
    public bool IsAssigneeUser { get; set; }
  }

  public class WorkflowVariableResultBase : WorkflowVariableBase
  {
    public Guid? Id { get; set; }
    public virtual IEnumerable<string> TriggerNames { get; set; }
    public string State { get; set; }
  }
}

using System;
using tomware.Microwf.Core.Execution;

namespace tomware.Microwf.Core.Definition
{
  public class Transition : FrontEndTransition
  {
    /// <summary>
    /// Decision point if a possible Transition can be made.
    /// </summary>
    public Func<TransitionContext, bool> CanMakeTransition { get; set; }

    /// <summary>
    /// Extension hook before the TargetState is beeing set.
    /// </summary>
    public Action<TransitionContext> BeforeTransition { get; set; }

    /// <summary>
    /// Extension hook after the TargetState has been set.
    /// </summary>
    public Action<TransitionContext> AfterTransition { get; set; }

    /// <summary>
    /// Extension hook to be set if a follow up Transition needs to be scheduled.
    /// </summary>
    public Func<TransitionContext, AutoTrigger> AutoTrigger { get; set; }

    public Transition()
    {
      CanMakeTransition = transitionContext => true;
    }
  }

  public class FrontEndTransition
  {
    public FrontEndTransition()
    {
    }
  
    public FrontEndTransition(Transition transition)
    {
      State = transition.State;
      Trigger = transition.Trigger;
      TargetState = transition.TargetState;
      Assignee = transition.Assignee;
      IsAssigneeUser = transition.IsAssigneeUser;
      SendBackToRequester = transition.SendBackToRequester;
      SendBackToHandler = transition.SendBackToHandler;
      SetUserAsHandler = transition.SetUserAsHandler;
    }
  
    /// <summary>
    /// Current state before trigger happens.
    /// </summary>
    public string State { get; set; }

    /// <summary>
    /// Trigger that fires the transition.
    /// </summary>
    public string Trigger { get; set; }

    /// <summary>
    /// Target state after a successful transition.
    /// </summary>
    public string TargetState { get; set; }

    public string Assignee { get; set; }
    
    public bool IsAssigneeUser { get; set; }
    
    public bool SendBackToRequester { get; set; }
    
    public bool SendBackToHandler { get; set; }
    
    public bool SetUserAsHandler { get; set; }

  }
}
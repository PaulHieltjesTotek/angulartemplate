using System;

namespace tomware.Microwf.Core.Definition
{
  public class AutoTrigger
  {
    public string Trigger { get; set; }

    public DateTime? DueDate { get; set; }
  }
}